// Colors - best in HSLA
export const color = {
  // Shades
  black: 'hsla(344, 6%, 13%, 1.0)', //'hsla(0, 0%, 0%, 1.0)',
  white: 'hsla(0, 0%, 100%, 1.0)',
  gray: 'hsla(0, 0%, 71%, 1.0)',
  lightgray: 'hsla(0, 0%, 82%, 1.0)',
  xlightgray: 'hsla(0, 0%, 95%, 1.0)',

  // Primaries
  red: 'hsla(359, 60%, 58%, 1.0)',
  green: 'hsla(137, 74%, 42%, 1.0)',
  orange: 'hsla(27, 90%, 59%, 1.0)',
  blue: 'hsla(200, 80%, 48%, 1.0)',
  lightblue: 'hsla(201, 66%, 74%, 1.0)',
  darkblue: 'hsla(210, 72%, 39%, 1.0)',

  // Theme specific
  primary: 'hsla(200, 80%, 48%, 1.0)',
  primarydark: 'hsla(200, 80%, 38%, 1.0)',
  primarylight: 'hsla(200, 80%, 58%, 1.0)',
  primarysuperlight: 'hsla(200, 80%, 88%, 1.0)',
  primaryoffset: 'hsla(210, 87%, 34%, 1.0)',
  secondary: 'hsla(201, 66%, 74%, 1.0)',
  secondarysuperlight: 'hsla(206, 67%, 84%, 1.0)',
}
