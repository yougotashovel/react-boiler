// Theme variables
// - anything specific to the theme,
// - that is not generic to all CSS.

export const heights = {
  header: 120, // example - header height
  header_s: 50, // example - header height for s_down breakpoint

  page: '2em', // page margin
  page_s: '0', // page margin s_down
}
