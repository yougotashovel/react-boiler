// Set Breakpoints - used below.
export const breakpoints = {
  s: 400,
  m: 650,
  l: 800,
  xl: 1200,
  xxl: 1400,
}

// All media breakpoint in CSS used across the app.
//   X_only = between these two breakpoints
//   X_down = this breakpoint and smaller screens
//   X_up = this breakpoint and larger screens
export const breakpoint = {
  xs_only : `@media only screen and (max-width: ${breakpoints.s}px)`,
  s_only  : `@media only screen and (min-width: ${breakpoints.s+1}px) and (max-width: ${breakpoints.m}px)`,
  m_only  : `@media only screen and (min-width: ${breakpoints.m+1}px) and (max-width: ${breakpoints.l}px)`,
  l_only  : `@media only screen and (min-width: ${breakpoints.l+1}px) and (max-width: ${breakpoints.xl}px)`,
  xl_only : `@media only screen and (min-width: ${breakpoints.xl+1}px)`,

  xs_down : `@media only screen and (max-width: ${breakpoints.s}px)`,
  s_down  : `@media only screen and (max-width: ${breakpoints.m}px)`,
  m_down  : `@media only screen and (max-width: ${breakpoints.l}px)`,
  l_down  : `@media only screen and (max-width: ${breakpoints.xl}px)`,

  s_up    : `@media only screen and (min-width: ${breakpoints.s+1}px)`,
  m_up    : `@media only screen and (min-width: ${breakpoints.m+1}px)`,
  l_up    : `@media only screen and (min-width: ${breakpoints.l+1}px)`,
  xl_up   : `@media only screen and (min-width: ${breakpoints.xl+1}px)`,
}

// const makeMediaQueries = (breakpoints) => {
//   const queries = {}
//   const keys = Object.keys(breakpoints)
//   for (var i = 0; i < keys.length; i++) {
//     queries[`${keys[i]}_down`] = `@media only screen and (max-width: ${breakpoints[keys[i-1]]}px)`
//     queries[`${keys[i]}_up`]   = `@media only screen and (min-width: ${breakpoints[keys[i]] + 1}px)`
//     queries[`${keys[i]}_only`] = `@media only screen and (min-width: ${breakpoints[keys[i]] + 1}px) and (max-width: ${breakpoints[keys[i-1]]}px)`
//     if (i===0) { delete queries[`${keys[i]}_`]; }
//     if (i===keys.length-1) {}
//   }
//   return queries
// }
// makeMediaQueries(breakpoints)
