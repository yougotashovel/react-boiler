import settings from '../settings'

export const speed = {
  // Speed in miliseconds
  1: 75,
  2: 150,
  3: 300,
  4: 500,
  5: 1000,
}

export const transition = {
  // Basic transition css values - Don't forget to add the unit of time.
  // Can turn all transitions off in settings - helpful for debugging.
  xfast: settings.transitions.on ? `all ${speed[1]}ms ease` : 'none',
  fast:  settings.transitions.on ? `all ${speed[2]}ms ease` : 'none',
  norm:  settings.transitions.on ? `all ${speed[3]}ms ease` : 'none',
  slow:  settings.transitions.on ? `all ${speed[4]}ms ease` : 'none',
  xslow: settings.transitions.on ? `all ${speed[5]}ms ease` : 'none',
}
