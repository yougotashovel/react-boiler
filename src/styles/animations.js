import { keyframes } from 'react-emotion'

export const fadeIn = keyframes({
  '0%': { opacity: 0, },
  '100%': { opacity: 1, },
})

export const fadeRight = keyframes({
  '0%': { opacity: 0, transform: 'translateX(20px)' },
  '100%': { opacity: 1, transform: 'translateX(0px)' },
})

export const fadeLeft = keyframes({
  '0%': { opacity: 0, transform: 'translateX(-20px)' },
  '100%': { opacity: 1, transform: 'translateX(0px)' },
})

export const fadeUp = keyframes({
  '0%': { opacity: 0, transform: 'translateY(20px)' },
  '100%': { opacity: 1, transform: 'translateY(0px)' },
})

export const fadeDown = keyframes({
  '0%': { opacity: 0, transform: 'translateY(-20px)' },
  '100%': { opacity: 1, transform: 'translateY(0px)' },
})

export const spin = keyframes({
  '0%': { transform: 'rotate(0deg)' },
  '100%': { transform: 'rotate(360deg)' },
})
