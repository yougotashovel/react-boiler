// Z Manager - keep track of all 'z-index' values - no more guessing.
export const z = {
  bg: -1,
  header: 100,
  nav: 110,
  modal: 120,
}
