import React from 'react';
import { connect } from 'react-redux';

import View from 'components/View/View';
import { Grid, Col } from 'components/Grid';
import FadeIn from '../components/Animated/FadeIn';

const Transitions = ({ location }) => (
  <View width="1000px">

    <Grid>
    <Col>
      <FadeIn up delay="100ms">
      <View transition width="25%" background="red" padding="1em">
        Panel 1
      </View>
      </FadeIn>
    </Col>
    <Col>
      <FadeIn up delay="000ms">
      <View transition width="50%" background="yellow" padding="1em">
        Panel 2
      </View>
      </FadeIn>
    </Col>
    <Col>
      <FadeIn up delay="200ms">
      <View transition width="25%" background="orange" padding="1em">
        Panel 3
      </View>
      </FadeIn>
    </Col>

    </Grid>

  </View>
);

const mapState = state => ({
  location: state.location.type,
  kind: state.location.kind,
})

const mapDispatch = dispatch => ({
  
})

export default connect(mapState, mapDispatch)(Transitions);
