import styled from 'react-emotion';
import * as css from 'styles'

import View from 'components/View/View';

const Nav = styled(View)({
  '& a':{
    transition: css.transition.fast,

    color: css.color.primary,
    borderBottom: `${css.border.solid} transparent`,
    marginRight: css.space.xs,

    '&:hover, &:focus':{
      color: css.color.primary,
      borderColor: css.color.primary,
    },

    '&.active':{
      color: css.color.primarydark,
      borderColor: css.color.primarydark,
    },
  }
})

export default Nav;
