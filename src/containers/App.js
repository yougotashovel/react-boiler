import React from 'react';
import { connect } from 'react-redux';

import content from '../data/content.json'

import { Grid, Col } from 'components/Grid'
import Meta from 'components/Meta/Meta';
import View from 'components/View/View';
import Text from 'components/Text/Text';
import Link from 'components/Link/Link';
import FadeIn from '../components/Animated/FadeIn';

import Nav from 'containers/Nav';
import Counter from 'containers/Counter';
import Posts from 'containers/Posts';
import Photos from 'containers/Photos/Photos';
import SinglePhoto from 'containers/Photos/SinglePhoto';

import Products from 'containers/Shop/Products';
import Cart from 'containers/Shop/Cart';
import Checkout from 'containers/Shop/Checkout';
import Home from './Home';
import Transitions from './Transitions';


const App = ({ location, kind, cartItems }) => (
  <FadeIn>
  <View padding="2em">

    <Meta defaults />

    <Nav padding="0 0 20px 0">
      <Link to="/">Home</Link>
      <Link nav to="/photos">Photos</Link>
      <Link nav to="/posts">Posts</Link>
      <Link nav to="/counter">Counter</Link>
      <Link nav to="/transitions">Transitions</Link>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <Link nav to="/products">Products</Link>
      <Link nav to="/cart">Cart ({cartItems})</Link>
      <Link nav to="/checkout">Checkout</Link>
    </Nav>

    {location==='route/home' && <Home content={content.pages.home}/>}
    {location==='route/posts' && <Posts/>}
    {(location==='route/photos'||location==='route/photo') && <Photos/>}
    {/* {location==='route/photo' && <FadeIn left delay={1000}><SinglePhoto/></FadeIn>} */}
    {location==='route/counter' && <Counter/>}
    {location === 'route/transitions' && <Transitions />}

    {location==='route/products' && <Products/>}
    {location==='route/cart' && <Cart/>}
    {location==='route/checkout' && <Checkout/>}

  </View>
  </FadeIn>
);

const mapState = state => ({
  location: state.location.type,
  kind: state.location.kind,
  cartItems: Object.keys(state.cart.items).reduce((total, item) => total+state.cart.items[item], 0),
})

export default connect(mapState)(App);
