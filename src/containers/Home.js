import React from 'react';
import * as css from 'styles'

import View from 'components/View/View';
import Button from 'components/Button/Button';
import Text from 'components/Text/Text';
import FadeIn from '../components/Animated/FadeIn';
import Link from '../components/Link/Link';
import List from '../components/List/List'

const Home = ({ content }) => (
  <View>

    <FadeIn down>
      <Text type="h1">{content.title}</Text>
    </FadeIn>

    <FadeIn delay="200ms">
      <Text type="h4">{content.subtitle}</Text>
    </FadeIn>

    <FadeIn delay="300ms">
    <View padding={`${css.space.r} 0`}>
      <Text type="h3">Data</Text>
      <List>
        {content.features_data.length>0 && content.features_data.map((feature)=>(
          <li>{feature}</li>
        ))}
      </List>

      <Text type="h3">Styles</Text>
      <List>
        {content.features_styles.length > 0 && content.features_styles.map((feature) => (
          <li>{feature}</li>
        ))}
      </List>
      
      <Text type="h3">Components</Text>
      <List>
        {content.features_components.length > 0 && content.features_components.map((feature) => (
          <li>{feature}</li>
        ))}
      </List>

      <Text type="h3">Common Patterns</Text>
      <List>
        {content.features_patterns.length > 0 && content.features_patterns.map((feature) => (
          <li>{feature}</li>
        ))}
      </List>

    </View>

    <View padding={`${css.space.r} 0`}>
      <Link href="/cart/?sku1=2&sku3=1">
        <Button small>Auto Populate Cart</Button>
      </Link>
    </View>
    </FadeIn>

  </View>
);


export default Home;
