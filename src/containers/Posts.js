import React from 'react';
import { connect } from 'react-redux';
import { select } from '@rematch/select';
import { getVisibleData } from 'redux/models/example.posts'

import Input from 'components/Input/Input';
import Button from 'components/Button/Button';
import Text from 'components/Text/Text';
import View from 'components/View/View';
import FadeIn from 'components/Animated/FadeIn'
import Loading from 'components/Loading/Loading';

const Posts = (props) => (
  <View>

    <FadeIn right>
      <Text type="h1">Posts</Text>
    </FadeIn>

    <FadeIn left delay="300ms">
      <View inlineBlock>
        {/* <Button small onClick={props.fetch}>fetch</Button> */}
        {/* <Button small onClick={props.reset}>reset</Button> */}
        {/* <Button small onClick={() => props.fetchPost(2)}>fetch single</Button> */}

        <Input
          type="text"
          placeholder="Search..."
          value={props.query.search ? props.query.search : ''}
          onChange={props.search}
        />
      </View>

      <View inlineBlock>
        <Loading
          loading={props.loading}
          loaded={props.loaded}
          error={props.error}
          small
        />
      </View>
    </FadeIn>


    {props.data.map((post,i)=>(
      <FadeIn key={i} delay={`${250+(i*15)}ms`}>
        <View margin="0.5em 0" padding="0.5em" background="hsla(100, 0%, 50%, 1)" color="#fff">
          <Text type="h4">{post.id}: {post.title}</Text>
        </View>
      </FadeIn>
    ))}

  </View>
);

const mapState = state => {
  console.log(state.posts, select.posts,
    // select.posts.filterData([{title: 'poop'}, {title: 'wee'}])
  );

  return {
    data: getVisibleData(state.posts),
    // data: select.posts.getVisibleData(state.posts),
    loading: state.posts.loading,
    loaded: state.posts.loaded,
    error: state.posts.error,
    query: state.posts.query,
  }
}

const mapDispatch = dispatch => ({
  fetch: dispatch.posts.fetch,
  reset: dispatch.posts.reset,
  fetchPost: (id) => dispatch.post.fetch({id}),
  setFilter: (filter, value) => dispatch.posts.setFilter({filter, value}),
  search: (event) => dispatch.posts.setFilter({filter: 'search', value: event.target.value}),
})

export default connect(mapState, mapDispatch)(Posts);
