import React from 'react';
import { connect } from 'react-redux';

import View from 'components/View/View';
import Button from 'components/Button/Button';
import Text from 'components/Text/Text';
import Modal from '../components/Modal/Modal';
import FadeIn from '../components/Animated/FadeIn';
import Img from '../components/Img/Img';

const Counter = (props) => (
  <View>

    <FadeIn down>
      <Text type="h1">Count: {props.count}</Text>
    </FadeIn>

    <FadeIn delay="200ms">
      <Button onClick={props.increment}>+1</Button>
      <Button onClick={props.incrementAsync}>+1 with delay</Button>
      <Button onClick={() => props.openModal('test')}>Modal 1</Button>
      <Button onClick={() => props.openModal('test2')}>Modal 2</Button>
    </FadeIn>

    <Modal name="test">
      <FadeIn up delay="0ms">
        <Text type="h2">Hello</Text>
      </FadeIn>

      <FadeIn up delay="100ms">
        <Text>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita labore sunt ipsam. Quia a rem rerum quo saepe, vero unde?</Text>
      </FadeIn>

      <br/><br/>
      <FadeIn up delay="200ms">
        <Button onClick={() => props.confirmModal('test')} small>Confirm</Button>
        <Button onClick={() => props.closeModal('test')} small outlined>Dismiss</Button>
      </FadeIn>
    </Modal>

    <Modal name="test2">
      <Img
        src={`//source.unsplash.com/random/400x400/?profile&num=560`}
        height="400px"
        nospinner
      />

      <FadeIn up delay="0ms">
        <Text type="h2">Different</Text>
      </FadeIn>

      <br/><br/>
      <FadeIn up delay="200ms">
        <Button onClick={() => props.confirmModal('test2')} small>Confirm</Button>
        <Button onClick={() => props.closeModal('test2')} small outlined>Dismiss</Button>
      </FadeIn>
    </Modal>

  </View>
);

const mapState = state => ({
  count: state.counter
})

const mapDispatch = dispatch => ({
  increment: () => dispatch.counter.increment(1),
  incrementAsync: () => dispatch.counter.incrementAsync(1),
  openModal: (name) => dispatch.modals.open({name}),
  closeModal: (name) => dispatch.modals.close({name}),
  confirmModal: (name) => { dispatch.modals.confirm({name}); dispatch.counter.increment(1); },
})

export default connect(mapState, mapDispatch)(Counter);
