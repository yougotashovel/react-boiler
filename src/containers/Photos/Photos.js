import React from 'react';

import Nav from './PhotosNav'
import List from './PhotosList'

import Text from 'components/Text/Text';
import View from 'components/View/View';
import FadeIn from 'components/Animated/FadeIn';

const Photos = (props) => (
  <View>

    <FadeIn up>
      <Text type="h1">Photos</Text>
    </FadeIn>

    <FadeIn delay="200ms">
      <Nav/>
      <List/>
    </FadeIn>

  </View>
);

export default Photos;
