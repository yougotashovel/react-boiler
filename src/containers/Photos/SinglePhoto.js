import React from 'react';
import { connect } from 'react-redux';

import Text from 'components/Text/Text';
import View from 'components/View/View';
import Img from 'components/Img/Img';
import { Grid, Col } from 'components/Grid'

// const ListItem = ({ item }) => (
class Single extends React.PureComponent { // Needs to be PureComponent because Link will cause re-render.
  // componentDidMount() {
  //   this.props.fetch({id: this.props.id})
  // }

  render() {
    const { item } = this.props
    console.log(item);
    return (
      <View margin="0.5em 0" padding="0" background="hsla(100, 0%, 90%, 1)">
        <Grid>
          <Col xs="u12" s="u12" m="u6" l="u6" xl="u6">
            <View padding="1em">
            <Text type="h2" color="red">{item.first_name} {item.last_name}</Text>
            <Text type="h3" color="blue">{item.email}</Text>
            <Text type="h5" color="green">{item.gender}</Text>
            <Text type="h5" color="orange">{item.job}</Text>
            </View>
            <View margin="1em">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident assumenda ut sed ducimus iusto qui aperiam voluptate praesentium perspiciatis mollitia vero quidem, nesciunt, debitis maxime vitae impedit quod aliquid enim beatae ipsum corporis, sit delectus. Iusto eaque sequi porro unde, quisquam provident odit voluptatibus deleniti aliquid fugit obcaecati repellendus at laudantium, beatae vero eveniet quos quis non, asperiores a. Distinctio.
            </View>
          </Col>
          <Col xs="u12" s="u12" m="u6" l="u6" xl="u6">
            <Img
              src={`//source.unsplash.com/random/400x400/?profile`}
              height="300px"
              nospinner
            />
          </Col>
          <Col xs="u12" s="u12" m="u12" l="u12" xl="u12">
            <View margin="1em">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident assumenda ut sed ducimus iusto qui aperiam voluptate praesentium perspiciatis mollitia vero quidem, nesciunt, debitis maxime vitae impedit quod aliquid enim beatae ipsum corporis, sit delectus. Iusto eaque sequi porro unde, quisquam provident odit voluptatibus deleniti aliquid fugit obcaecati repellendus at laudantium, beatae vero eveniet quos quis non, asperiores a. Distinctio.
            </View>
          </Col>
        </Grid>
      </View>
    )
  }
}
// )

const mapState = state => ({
  item: state.photo.data,
  id: state.location.payload.id,
})

const mapDispatch = dispatch => ({
  fetch: dispatch.photo.fetch,
})

export default connect(mapState, mapDispatch)(Single)
