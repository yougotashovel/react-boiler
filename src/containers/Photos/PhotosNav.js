import React from 'react';
import { connect } from 'react-redux';

import Loading from 'components/Loading/Loading';
import Input from 'components/Input/Input';
import Button from 'components/Button/Button';
import View from 'components/View/View';

const filters = [
  { filter: 'gender', value: 'female', label: 'Female' },
  { filter: 'gender', value: 'male', label: 'Male' },
  { filter: 'search', value: 'justin', label: 'justin' },
  { filter: 'search', value: 'bella', label: 'bella' },
]

const Nav = (props) => (
  <View>

    {/* <View>
      <Button small onClick={props.fetch}>fetch</Button>
      <Button small onClick={props.reset}>reset</Button>
    </View> */}

    <View margin="0.5em 0 0 0">
      {filters.map((filter,i)=>(
        <Button
          key={i}
          small
          active={props.query[filter.filter] === filter.value}
          onClick={props.setFilter(filter.filter, filter.value)}
        >{filter.label}</Button>
      ))}

      <Button small onClick={props.resetFilters}>Reset</Button>

      <Input
        type="text"
        placeholder="Search..."
        value={props.query.search ? props.query.search : ''}
        onChange={props.search}
      />
    </View>

    <View inlineBlock>
      <Loading
        loading={props.loading}
        loaded={props.loaded}
        error={props.error}
        small
      />
    </View>

  </View>
);

const mapState = state => ({
  loading: state.photos.loading,
  loaded: state.photos.loaded,
  error: state.photos.error,
  query: state.photos.query,
})

const mapDispatch = dispatch => ({
  fetch: dispatch.photos.fetch,
  reset: dispatch.photos.reset,
  setFilter: (filter, value) => () => dispatch.photos.setFilter({filter, value}),
  search: (event) => dispatch.photos.setFilter({filter: 'search', value: event.target.value}),
  resetFilters: dispatch.photos.resetFilters,
})

export default connect(mapState, mapDispatch)(Nav);
