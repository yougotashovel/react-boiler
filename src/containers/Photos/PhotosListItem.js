import React from 'react';
import { connect } from 'react-redux';
import posed from 'react-pose';
import getRandomInt from '../../utils/getRandomInt';

import Link from 'components/Link/Link';
import Text from 'components/Text/Text';
import View from 'components/View/View';
import Img from 'components/Img/Img';
import FadeIn from '../../components/Animated/FadeIn';
import { Grid, Col } from 'components/Grid'

const Box = posed(View)({
  initialPose: 'closed',
  open: { opacity: 1, scale: 1.05, zIndex: 100, position: 'relative' },
  closed: { opacity: 1, scale: 1, zIndex: 1 },
})

// const ListItem = ({ item }) => (
class ListItem extends React.PureComponent { // Needs to be PureComponent because Link will cause re-render.
  state = { open: false }
  toggle = (e) => {
    e.preventDefault();
    requestAnimationFrame(() => {
      this.setState({ open: !this.state.open })
    })
  }

  render() {
    const { item } = this.props
    if (this.props.open) {
      return (
        <Link  to={`/photos`}>
        <Box pose="open" transition margin="0.5em 0" padding="0em" background="#f00f00" color="#fff" style={{
          // position: 'fixed',
          // top: 0,
          // left: 0,
          // right: 0,
          // bottom: 0,
          // zIndex: 100,
          // width: '100%',
          // height: '100%',
        }}>
          <Grid>
            <Col xs="u12" s="u12" m="u6" l="u8" xl="u8">
              <View padding="1em">
              <Text key="item-title" type="h2" color="white">{item.first_name} {item.last_name}</Text>
              {/* <View transition opacity="0" height="0px">
                <Text type="h4" color="white">{item.email}</Text>
                <Text type="h5" color="hsla(0,0%,100%,0.75)">{item.gender}</Text>
                <Text type="h5" color="hsla(0,0%,100%,0.75)">{item.job}</Text>
              </View> */}

              <View transition margin="0.5em 0 0 0">
                <FadeIn down delay="500ms">
                  Consectetur adipisicing elit. Suscipit, minus deserunt nostrum in facilis molestias ad labore unde ipsum accusantium!
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim fugit veniam dignissimos quo deserunt, consequuntur rerum illum veritatis laudantium iusto, qui nisi maxime eius? Atque odit harum modi quod, corporis assumenda officiis obcaecati commodi laborum esse ipsum vitae repellendus sapiente.
                </FadeIn>
              </View>
              </View>
            </Col>
            <Col xs="u12" s="u12" m="u6" l="u4" xl="u4">
              <Img
                src={`//source.unsplash.com/random/400x400/?profile&num=${item.id}`}
                height="400px"
                nospinner
              />
            </Col>
            <Col xs="u12" s="u12" m="u6" l="u12" xl="u12">
            </Col>
          </Grid>
        </Box>
        </Link>
      )
    } else {
      return (
        <Link to={`/photos/${item.id}`}>
        <Box pose="closed" transition margin="0.5em 0" padding="0" background="hsla(100, 0%, 90%, 1)" color="#fff" style={{
          // position: 'absolute',
        }}>
          <Grid>
            <Col xs="u12" s="u12" m="u6" l="u8" xl="u8">
              <View padding="1em">
              <Text key="item-title" type="h2" color="red">{item.first_name} {item.last_name}</Text>
              <View transition height="60px">
                <FadeIn delay="500ms">
                  <Text type="h4" color="blue">{item.email}</Text>
                  <Text type="h5" color="green">{item.gender}</Text>
                  <Text type="h5" color="orange">{item.job}</Text>
                </FadeIn>
              </View>
              </View>
            </Col>
            <Col xs="u12" s="u12" m="u6" l="u4" xl="u4">
              <Img
                src={`//source.unsplash.com/random/400x400/?profile&num=${item.id}`}
                height="140px"
                nospinner
              />
            </Col>
            <Col xs="u12" s="u12" m="u6" l="u12" xl="u12">
            </Col>
          </Grid>
        </Box>
        </Link>
      )
    }

  }
}
// )

const mapState = (state, props) => ({
  open: state.location.payload.id === props.item.id
})

export default connect(mapState)(ListItem)
