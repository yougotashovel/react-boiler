import React from 'react';
import { connect } from 'react-redux';
import { select } from '@rematch/select';
import { getVisibleData } from 'redux/models/example.photos'

import VirtualList from 'react-tiny-virtual-list'

// import Loading from 'components/Loading/Loading';
import View from 'components/View/View'
import FadeIn from 'components/Animated/FadeIn'
import ListItem from './PhotosListItem'

const List = (props) => (
  <View>

    {/* <View inlineBlock>
      <Loading
        loading={props.loading}
        loaded={props.loaded}
        error={props.error}
        small
      />
    </View> */}

    {/* <VirtualList
      width="100%"
      height={600}
      itemCount={props.data.length}
      itemSize={231} // Also supports variable heights (array or function getter)
      renderItem={({index, style}) =>
        <div key={index} style={style}>
          <ListItem item={props.data[index]}/>
        </div>
      }
    /> */}

    {props.data.map((item,i)=> i<3 && (
      <FadeIn key={item.email} delay={`${i*15}ms`}>
        <ListItem item={item}/>
      </FadeIn>
    ))}

  </View>
);


const mapState = state => ({
  data: getVisibleData(state.photos),
  // data: select.posts.getVisibleData(state.posts),
  loading: state.photos.loading,
  loaded: state.photos.loaded,
  error: state.photos.error,
})

const mapDispatch = dispatch => ({})

export default connect(mapState, mapDispatch)(List);
