import React from 'react';
import { connect } from 'react-redux';

import { Grid, Col } from 'components/Grid';
import Text from 'components/Text/Text';
import View from 'components/View/View';
import FadeIn from 'components/Animated/FadeIn';
import Button from 'components/Button/Button';

import CheckoutProgress from './CheckoutProgress';
import CheckoutPanel from './CheckoutPanel';
// import {  } from 'redux/models/example.checkout';
import Totals from './Totals';
import CartSummary from './CartSummary'

const Checkout = ({ panels, currentPanel, }) => (
  <View>

    <FadeIn left>
      <Text type="h1">Checkout</Text>
    </FadeIn>

    <Grid gutter="3em">

      <Col xs="u12" s="u12" m="u8" l="u8" xl="u8">

        {/* <CheckoutProgress/> */}

        {/* .filter(panel=>panel===currentPanel) */}
        {Object.keys(panels).map((id,i)=>{
          const panel = panels[id]
          return <CheckoutPanel key={id} id={id} panel={panel}/>
        })}

      </Col>

      <Col xs="u12" s="u12" m="u4" l="u4" xl="u4">

        <Totals/>

        <CartSummary/>

      </Col>

    </Grid>

  </View>
)

const mapState = state => ({
  panels: state.checkout.panels,
  currentPanel: state.checkout.currentPanel,
})

const mapDispatch = dispatch => ({

})

export default connect(mapState, mapDispatch)(Checkout)
