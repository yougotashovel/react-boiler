import React from 'react';
import { connect } from 'react-redux';

import Text from 'components/Text/Text';
import View from 'components/View/View';
import FadeIn from 'components/Animated/FadeIn';
import Button from 'components/Button/Button';

import ListItem from './CartSummaryItem';
import { cartItemsCount, getSubtotal } from 'redux/models/example.cart';
import Totals from './Totals';

const Cart = ({ products, items, subtotal, cartItemsCount }) => (
  <FadeIn>
  <View margin="2em 0">

    {Object.keys(items).map((item, i)=>{
      var product = products.filter(product=>product.id===parseInt(item))[0]
      return <ListItem key={product.id} item={product} />
    })}

  </View>
  </FadeIn>
)

const mapState = state => ({
  products: state.products.data,
  items: state.cart.items,
  subtotal: 0, // getSubtotal(state.cart.items, state.products.data),
  cartItemsCount: cartItemsCount(state.cart.items),
})

const mapDispatch = dispatch => ({

})

export default connect(mapState, mapDispatch)(Cart)
