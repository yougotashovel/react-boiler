import React from 'react';
import { connect } from 'react-redux';

import { Grid, Col } from 'components/Grid';
import View from 'components/View/View';
import Text from 'components/Text/Text';
import FadeIn from 'components/Animated/FadeIn';
import ListItem from './ProductsListItem';

const Products = ({ products }) => (
  <View>

    <FadeIn right>
      <Text type="h1">Products</Text>
    </FadeIn>

    <Grid gutters>
      {products.map((item, i)=>(
        <Col key={item.id}>
          <ListItem  item={item} />
        </Col>
      ))}
    </Grid>

  </View>
)

const mapState = state => ({
  products: state.products.data,
})

const mapDispatch = dispatch => ({

})

export default connect(mapState, mapDispatch)(Products)
