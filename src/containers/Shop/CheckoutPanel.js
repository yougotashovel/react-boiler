import React from 'react';
import { connect } from 'react-redux';

import { Grid, Col } from 'components/Grid';
import Text from 'components/Text/Text';
import View from 'components/View/View';
import FadeIn from 'components/Animated/FadeIn';
import Button from 'components/Button/Button';
import Link from 'components/Link/Link';

import Login from './CheckoutFormLogin';
import Shipping from './CheckoutFormShipping';
import Billing from './CheckoutFormBilling';
// import Confirm from './CheckoutPanelConfirm';
// import Complete from './CheckoutPanelComplete';

const CheckoutPanel = ({
  id,
  panel,
  goToPanel,
  goToNextPanel,
  currentPanel,

  // User data
  email,
  shipping,
  billing,
}) => (
  <FadeIn>
  <View
    padding="1em"
    margin="0.5em 0 1.5em 0"
    background="hsla(100,0%,95%,1)"
    onClick={()=>currentPanel!==id && goToPanel(id)}
    >

    <Grid>

      <Col xs="u12" s="u12" m="u3" l="u2" xl="u2">
        <Text type="h4" color="gray">{panel.title} {panel.validated && '✔'}</Text>
      </Col>
      <Col xs="u12" s="u12" m="u9" l="u9" xl="u9">
        {id === currentPanel &&
          <FadeIn delay="50ms">
          <View>
            {id==='login' && <Login id={id} panel={panel} goToNextPanel={goToNextPanel}/>}
            {id==='shipping' && <Shipping id={id} panel={panel} goToNextPanel={goToNextPanel}/>}
            {id==='billing' && <Billing id={id} panel={panel} goToNextPanel={goToNextPanel}/>}
          </View>
          </FadeIn>
        }

        {id !== currentPanel &&
          <FadeIn delay="50ms">
          <Text color="hsla(100,0%,40%,1)">
            {id==='login' ? email ? email : 'Enter your email' : ''}
            {id==='shipping' ? shipping.line_1 ? `${shipping.line_1}` : 'Enter delivery address' : ''}
            {id==='billing' && 'Enter payment info'}
            {/* {id==='billing' ? (billing.expiry_date) ? billing.expiry_date : 'Enter payment info' : ''} */}
            {/* ${shipping.zipcode && ', '+shipping.zipcode}` */}

            <Text float="right" color="hsla(100,0%,60%,1)">&nbsp;&nbsp;Edit</Text>
          </Text>
          </FadeIn>
        }

      </Col>

    </Grid>

  </View>
  </FadeIn>
)

const mapState = state => ({
  currentPanel: state.checkout.currentPanel,
  email: state.checkout.user.email,
  shipping: state.checkout.user.shipping,
  billing: state.checkout.user.billing,
})

const mapDispatch = (dispatch, props) => ({
  goToPanel: () => dispatch.checkout.changePanel({id: props.id}),
  goToNextPanel: (id, validated, next) => dispatch.checkout.checkPanel({id, validated, next}),
})

export default connect(mapState, mapDispatch)(CheckoutPanel)
