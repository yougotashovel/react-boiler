import React from 'react';
import { connect } from 'react-redux';

import { withFormik } from 'formik';
import Yup from 'yup';

import { Grid, Col } from 'components/Grid';
import Text from 'components/Text/Text';
import View from 'components/View/View';
import FadeIn from 'components/Animated/FadeIn';
import Button from 'components/Button/Button';
import Input from 'components/Input/Input';

const CheckoutPanel = ({
  id,
  panel,

  // Formik props
  values,
  touched,
  errors,
  dirty,
  isSubmitting,
  handleChange,
  handleBlur,
  handleSubmit,
  handleReset,
}) => (
  <form onSubmit={handleSubmit}>

    <Grid gutter="0.5em">
      <Col xs="u12" s="u12" m="u12" l="u12" xl="u12">
        <Input name="line_1" type="text" placeholder="Address Line 1" value={values.line_1}
        onChange={handleChange} onBlur={handleBlur}  errors={touched.line_1 && errors}/>
      </Col>
      <Col xs="u12" s="u12" m="u12" l="u12" xl="u12">
        <Input name="line_2" type="text" placeholder="Address Line 2" value={values.line_2}
        onChange={handleChange} onBlur={handleBlur}  errors={touched.line_2 && errors}/>
      </Col>
      <Col xs="u6" s="u6" m="u6" l="u6" xl="u6">
        <Input name="city" type="text" placeholder="City" value={values.city}
        onChange={handleChange} onBlur={handleBlur}  errors={touched.city && errors}/>
      </Col>
      <Col xs="u6" s="u6" m="u6" l="u6" xl="u6">
        <Input name="state" type="text" placeholder="State / Region" value={values.state}
      onChange={handleChange} onBlur={handleBlur}  errors={touched.state && errors}/>
      </Col>
      <Col xs="u12" s="u12" m="u12" l="u12" xl="u12">
        <Text color="gray">Enter to calculate shipping</Text>
        <Input name="zipcode" type="text" placeholder="Zip / Postcode" value={values.zipcode}
      onChange={handleChange} onBlur={handleBlur}  errors={touched.zipcode && errors}/>
      </Col>
    </Grid>

    <Button type="submit" small disabled={isSubmitting || Object.keys(errors).length}>
      Continue to {panel.next}
    </Button>

  </form>
)

const setupForm = withFormik({
  displayName: 'ShippingForm', // helps with React DevTools

  mapPropsToValues: ({ shipping }) => ({
    line_1: shipping.line_1 || '',
    line_2: shipping.line_2 || '',
    city: shipping.city || '',
    state: shipping.state || '',
    zipcode: shipping.zipcode || '',
  }),

  validationSchema: Yup.object().shape({
    // first_name: Yup.string()
    //   .min(2, "C'mon, your name is longer than that")
    //   .required('First name is required.'),
    // last_name: Yup.string()
    //   .min(2, "C'mon, your name is longer than that")
    //   .required('Last name is required.'),
    line_1: Yup.string()
      .required('Street address is required.'),
    zipcode: Yup.string()
      .min(5, "Zip / Postcode is not long enough")
      .required('Zip / Postcode is required.'),
    city: Yup.string()
      .required('City is required.'),
  }),

  handleSubmit: (payload, {props, setSubmitting}) => {
    // console.log('should submit', payload, props);
    props.goToNextPanel(props.id, true, props.panel.next)
    props.setShipping({shipping: payload})
  },
});

const mapState = state => ({
  shipping: state.checkout.user.shipping,
})

const mapDispatch = dispatch => ({
  setShipping: dispatch.checkout.setShipping,
})

export default connect(mapState, mapDispatch)(setupForm(CheckoutPanel))
