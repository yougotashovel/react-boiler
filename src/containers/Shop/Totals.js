import React from 'react';
import { connect } from 'react-redux';

import Text from 'components/Text/Text';
import View from 'components/View/View';
import FadeIn from 'components/Animated/FadeIn';
import Button from 'components/Button/Button';
import Table from 'components/Table/Table';

import { cartItemsCount, getSubtotal, getTax, getTotal, withCurrency } from 'redux/models/example.cart';

const Totals = ({ products, items, totals }) => (
  <View>

    <Text type="h4">
      <Table style={{color: 'gray'}}>
        <tbody>
          <tr>
            <td>Items</td>
            <td>{withCurrency(totals.subtotal)}</td>
          </tr>
          <tr>
            <td>Tax</td>
            <td>{withCurrency(totals.tax)}</td>
          </tr>
          <tr>
            <td>Total</td>
            <td>{withCurrency(totals.total)}</td>
          </tr>
          {/* <tr>
            <td></td>
            <td></td>
          </tr> */}
        </tbody>
      </Table>
    </Text>



    {/* {Object.keys(items).map((item, i)=>{
      var product = products.filter(product=>product.id===parseInt(item))[0]
      return <ListItem key={product.id} item={product} />
    })} */}

  </View>
)

const mapState = state => ({
  products: state.products.data,
  items: state.cart.items,

  cartItemsCount: cartItemsCount(state.cart.items),

  totals: state.cart.totals
})

const mapDispatch = dispatch => ({

})

export default connect(mapState, mapDispatch)(Totals)
