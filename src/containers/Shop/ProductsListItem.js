import React from 'react';
import { connect } from 'react-redux';

import Text from 'components/Text/Text';
import View from 'components/View/View';
import FadeIn from 'components/Animated/FadeIn';
import Button from 'components/Button/Button';
import Img from 'components/Img/Img';
import Link from 'components/Link/Link';

// const ListItem = ({ item }) => (
class ListItem extends React.PureComponent { // Needs to be PureComponent because Link will cause re-render.
  render() {
    const { item, addToCart, removeFromCart, increase, decrease, quantity, isSoldOut } = this.props

    return (
      <FadeIn>
        <Img
          src={`//source.unsplash.com/random/400x400/?tshirt&num=${item.id}`}
          height="150px"
          nospinner
        />
      <View padding="1em" margin="0 0 0.5em 0" background="hsla(100,0%,95%,1)">
        <Text type="h3" color="red">{item.title}</Text>
        <Text type="h4" color="gray">{item.price}</Text>
        <View margin="0.5em 0">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus deserunt autem, praesentium, dolor iure quam.
        </View>
        <Button small onClick={addToCart} disabled={isSoldOut}>{isSoldOut ? 'Sold Out' : 'Add to Cart'}</Button>

        {quantity && <Link to={`/cart`}><Text type="h5" color="hsla(100,0%,70%,1)">{quantity} in cart</Text></Link> }
      </View>
      </FadeIn>
    )
  }
}
// )

const mapState = (state, props) => ({
  quantity: state.cart.items[props.item.id],
  isSoldOut: (props.item.stock && props.item.stock<1) || (state.cart.items[props.item.id] >= props.item.stock),
})

const mapDispatch = (dispatch, {item: { id } }) => ({
  addToCart: (qty) => dispatch.cart.increase({id, qty}),
  removeFromCart: () => dispatch.cart.remove({id}),
  increase: () => dispatch.cart.increase({id}),
  decrease: () => dispatch.cart.decrease({id}),
})

export default connect(mapState, mapDispatch)(ListItem)
