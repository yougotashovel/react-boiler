import React from 'react';
import { connect } from 'react-redux';

import { Grid, Col } from 'components/Grid';
import Text from 'components/Text/Text';
import View from 'components/View/View';
import FadeIn from 'components/Animated/FadeIn';
import Button from 'components/Button/Button';
import Img from 'components/Img/Img';

import { withCurrency } from 'redux/models/example.cart';

// const ListItem = ({ item }) => (
class ListItem extends React.PureComponent { // Needs to be PureComponent because Link will cause re-render.
  render() {
    const { item, addToCart, removeFromCart, increase, decrease, quantity } = this.props

    return (
      <FadeIn>
      <View padding="1em" margin="0.5em 0" background="hsla(100,0%,95%,1)">
        <Grid>

          <Col l="u2" xl="u2">
            <Img
              src={`//source.unsplash.com/random/400x400/?tshirt&num=${item.id}`}
              height="100px"
              width="100px"
              nospinner
            />
          </Col>
          <Col>
            <Text block type="h3" color="red">{item.title}</Text>
            <Text block type="h4" color="gray">{withCurrency(item.price)}</Text>
          </Col>
          <Col tar>
            <Button small nomargin onClick={decrease} disabled={quantity<=1}>-</Button>
            <Button small nomargin outlined style={{width: '50px'}}>{quantity}</Button>
            <Button small nomargin onClick={increase} disabled={item.stock && quantity>=item.stock}>+</Button>
          </Col>
          <Col tac l="u2" xl="u2">
            {/* <Text type="h4" color="gray">{withCurrency(item.price*quantity)}</Text> */}
            <Button small text>{withCurrency(item.price*quantity)}</Button>
          </Col>
          <Col tar l="u1" xl="u1">
            <Button small outlined onClick={removeFromCart}>&times;</Button>
          </Col>
        </Grid>
      </View>
      </FadeIn>
    )
  }
}
// )

const mapState = (state, props) => ({
  quantity: state.cart.items[props.item.id],
  // quantity: (state.cart.items[props.item.id]>props.item.stock) ? props.item.stock : state.cart.items[props.item.id],
})

const mapDispatch = (dispatch, {item: { id } }) => ({
  addToCart: (qty) => dispatch.cart.increase({id, qty}),
  removeFromCart: () => dispatch.cart.remove({id}),
  increase: () => dispatch.cart.increase({id}),
  decrease: () => dispatch.cart.decrease({id}),
})

export default connect(mapState, mapDispatch)(ListItem)
