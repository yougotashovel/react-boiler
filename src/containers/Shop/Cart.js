import React from 'react';
import { connect } from 'react-redux';

import { Grid, Col } from 'components/Grid';
import Text from 'components/Text/Text';
import View from 'components/View/View';
import FadeIn from 'components/Animated/FadeIn';
import Button from 'components/Button/Button';
import Link from 'components/Link/Link';

import ListItem from './CartListItem';
import { cartItemsCount, getSubtotal } from 'redux/models/example.cart';
import Totals from './Totals';

const Cart = ({ products, items, subtotal, cartItemsCount, message }) => (
  <View>

    <FadeIn left>
      <Text type="h1">Cart ({cartItemsCount})</Text>
    </FadeIn>

    {message && <View padding="1em" background="#259cda" color="white">{message}</View>}

    <Grid gutter="3em">

      <Col xs="u12" s="u12" m="u8" l="u8" xl="u8">
        {(products.length) ? Object.keys(items).map((item, i)=>{
          var product = products.filter(product=>product.id===parseInt(item))[0]
          return <ListItem key={product.id} item={product} />
        }) : null}
      </Col>

      <Col xs="u12" s="u12" m="u4" l="u4" xl="u4">
        <Totals/>

        <br/>
        <Link to={`/checkout`}>
          <Button small fit disabled={!Object.keys(items).length}>
            Continue to Checkout
          </Button>
        </Link>
      </Col>

    </Grid>

  </View>
)

const mapState = state => ({
  products: state.products.data,
  items: state.cart.items,
  subtotal: 0, // getSubtotal(state.cart.items, state.products.data),
  cartItemsCount: cartItemsCount(state.cart.items),
  message: state.cart.message,
})

const mapDispatch = dispatch => ({

})

export default connect(mapState, mapDispatch)(Cart)
