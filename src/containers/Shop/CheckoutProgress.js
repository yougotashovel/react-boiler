import React from 'react';
import { connect } from 'react-redux';

import { Grid, Col } from 'components/Grid';
import Text from 'components/Text/Text';
import View from 'components/View/View';
import FadeIn from 'components/Animated/FadeIn';
import Button from 'components/Button/Button';

// import {  } from 'redux/models/example.checkout';

const CheckoutProgress = ({ panels, currentPanel, goToPanel }) => (
  <View margin="0.5em 0 1.5em 0">

    {Object.keys(panels).map((id,i)=>{
      const panel = panels[id]
      return (
        <Button
          key={`progress-${id}`}
          small
          onClick={()=>goToPanel(id)}
          disabled={id!==currentPanel && !panel.validated}
          active={id===currentPanel}
        >{i+1}. {panel.title}</Button>
      )
    })}

  </View>
)

const mapState = state => ({
  panels: state.checkout.panels,
  currentPanel: state.checkout.currentPanel,
})

const mapDispatch = dispatch => ({
  goToPanel: (id) => dispatch.checkout.checkPanel({id}),
})

export default connect(mapState, mapDispatch)(CheckoutProgress)
