import React from 'react';
import { connect } from 'react-redux';

import { withFormik } from 'formik';
import Yup from 'yup';

import { Grid, Col } from 'components/Grid';
import Text from 'components/Text/Text';
import View from 'components/View/View';
import FadeIn from 'components/Animated/FadeIn';
import Button from 'components/Button/Button';
import Input from 'components/Input/Input';
import Loading from 'components/Loading/Loading';

import { withCurrency } from 'redux/models/example.cart';

const CheckoutPanel = ({
  id,
  panel,
  total,

  // Formik props
  values,
  touched,
  errors,
  dirty,
  isSubmitting,
  handleChange,
  handleBlur,
  handleSubmit,
  handleReset,
}) => (
  <form onSubmit={handleSubmit}>

    <Grid gutter="0.5em">
      <Col xs="u12" s="u12" m="u12" l="u12" xl="u12">
        <Input name="card_number" type="text" placeholder="Card Number" value={values.card_number}
        onChange={handleChange} onBlur={handleBlur}  errors={errors}/>
      </Col>
      <Col xs="u8" s="u8" m="u8" l="u8" xl="u8">
        <Input name="expiry_date" type="text" placeholder="Expiry Date" value={values.expiry_date}
        onChange={handleChange} onBlur={handleBlur}  errors={errors}/>
      </Col>
      <Col xs="u4" s="u4" m="u4" l="u4" xl="u4">
        <Input name="ccv" type="text" placeholder="CCV" value={values.ccv}
        onChange={handleChange} onBlur={handleBlur}  errors={errors}/>
      </Col>
    </Grid>

    <Button type="submit" small disabled={isSubmitting || Object.keys(errors).length}>
      Pay &nbsp;&nbsp;<Text color="rgba(255,255,255,0.75)">{withCurrency(total)}</Text>
    </Button>
    <Loading small inline loaded={!isSubmitting}/>

  </form>
)

const setupForm = withFormik({
  displayName: 'BillingForm', // helps with React DevTools

  mapPropsToValues: ({ billing }) => ({
    card_number: billing.card_number || '',
    expiry_date: billing.expiry_date || '',
    ccv: billing.ccv || '',
  }),

  validationSchema: Yup.object().shape({
    card_number: Yup.string()
      .required('Card number is required.'),
    expiry_date: Yup.date()
      // .min(5, "Zip / Postcode is not long enough")
      .required('Expiry date is required.'),
    ccv: Yup.string()
      .min(3, "CCV is not long enough")
      .max(3, "CCV is too long")
      .required('CCV is required.'),
  }),

  handleSubmit: (payload, {props, setSubmitting}) => {
    // console.log('should submit', payload, props);
    // props.goToNextPanel(props.id, true, props.panel.next)
    props.setBilling({billing: payload})
    setSubmitting(true);
    // 1. Do serverside validation
    // 2. Pass errors to each individual form - this is where it gets tricky because everything is separate forms.
    // 3. If validated - create stripe order on back-end.
    // 4. Return confirmation, e.g: goToNextPanel('complete')
    // 5. Delete billing info for security.
  },
});

const mapState = state => ({
  billing: {}, //state.checkout.user.billing,
  total: state.cart.totals.total,
})

const mapDispatch = dispatch => ({
  setBilling: dispatch.checkout.setBilling,
})

export default connect(mapState, mapDispatch)(setupForm(CheckoutPanel))
