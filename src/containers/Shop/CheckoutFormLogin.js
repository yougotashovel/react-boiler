import React from 'react';
import { connect } from 'react-redux';

import { withFormik } from 'formik';
import Yup from 'yup';

import { Grid, Col } from 'components/Grid';
import Text from 'components/Text/Text';
import View from 'components/View/View';
import FadeIn from 'components/Animated/FadeIn';
import Button from 'components/Button/Button';
import Input from 'components/Input/Input';

const CheckoutPanel = ({
  id,
  panel,
  checkEmail,
  hasAccount,
  isLoggedIn,
  goToNextPanel,

  // Formik props
  values,
  touched,
  errors,
  dirty,
  isSubmitting,
  handleChange,
  handleBlur,
  handleSubmit,
  handleReset,
}) => (
  <form onSubmit={handleSubmit}>

    <Text color="gray">Used to send your order confirmation.</Text>
    <Input name="email" type="email" placeholder="Email" value={values.email}
      onChange={(e)=>{ handleChange(e); checkEmail(e);}} onBlur={handleBlur} errors={touched.email && errors}/>

    {hasAccount &&
      <View>
        <Text color="gray">Oh, you already have an account! Login for fast checkout.</Text>
        <Input name="password" type="password" placeholder="Password" value={values.password}
          onChange={handleChange} onBlur={handleBlur} errors={touched.password && errors}/>
      </View>
    }

    <Button type="submit" small disabled={isSubmitting || Object.keys(errors).length}>
      Continue to {panel.next}
    </Button>

  </form>
)

const setupForm = withFormik({
  displayName: 'LoginForm', // helps with React DevTools

  mapPropsToValues: ({ email }) => ({
    email: email || '',
    password: '',
  }),

  validationSchema: Yup.object().shape({
    email: Yup.string()
      .email('Invalid email address')
      .required('Email is required!'),
  }),

  handleSubmit: (payload, {props, setSubmitting}) => {
    // console.log('should submit', payload, props);
    props.goToNextPanel(props.id, true, props.panel.next)
    props.setEmail({email: payload.email})
  },
});

const mapState = state => ({
  hasAccount: state.checkout.user.hasAccount,
  email: state.checkout.user.email,
})

const mapDispatch = dispatch => ({
  setEmail: dispatch.checkout.setEmail,
  checkEmail: (e) => dispatch.user.fetch({email: e.target.value}),
})

export default connect(mapState, mapDispatch)(setupForm(CheckoutPanel))
