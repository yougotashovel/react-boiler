import React from 'react';
import { connect } from 'react-redux';

import { Grid, Col } from 'components/Grid';
import Text from 'components/Text/Text';
import View from 'components/View/View';
import FadeIn from 'components/Animated/FadeIn';
import Button from 'components/Button/Button';
import Img from 'components/Img/Img';

import { withCurrency } from 'redux/models/example.cart';

// const ListItem = ({ item }) => (
class ListItem extends React.PureComponent { // Needs to be PureComponent because Link will cause re-render.
  render() {
    const { item, addToCart, removeFromCart, increase, decrease, quantity } = this.props

    return (
      <FadeIn>
      <View padding="0.5em" margin="0.25em 0" background="hsla(100,0%,95%,1)">
        <Grid>
          <Col xs="u2" s="u2" m="u2" l="u1" xl="u1">
            <Img
              src={`//source.unsplash.com/random/400x400/?tshirt&num=${item.id}`}
              height="30px"
              width="30px"
              nospinner
            />
          </Col>
          <Col>
            <Text type="h5" color="red">{item.title}</Text>
          </Col>
          {/* <Col>
            <Text type="h5" color="gray"> x {quantity}</Text>
          </Col> */}
          <Col tar>
            <Button xsmall text nomargin onClick={decrease} disabled={quantity<=1}>-</Button>
            <Button xsmall text nomargin outlined>{quantity}</Button>
            <Button xsmall text nomargin onClick={increase} disabled={item.stock && quantity>=item.stock}>+</Button>
          </Col>
          <Col tar>
            <Text type="h5" color="gray">{withCurrency(item.price*quantity)}</Text>
            <Button text xsmall nopadding nomargin onClick={removeFromCart}><Text type="h6" color="hsla(100,0%,70%,1)">Remove</Text></Button>
          </Col>
          {/* <Col tar>
            <Button small outlined onClick={removeFromCart}>&times;</Button>
          </Col> */}
        </Grid>
      </View>
      </FadeIn>
    )
  }
}
// )

const mapState = (state, props) => ({
  quantity: state.cart.items[props.item.id],
})

const mapDispatch = (dispatch, {item: { id } }) => ({
  addToCart: (qty) => dispatch.cart.increase({id, qty}),
  removeFromCart: () => dispatch.cart.remove({id}),
  increase: () => dispatch.cart.increase({id}),
  decrease: () => dispatch.cart.decrease({id}),
})

export default connect(mapState, mapDispatch)(ListItem)
