import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route } from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker'

import { Provider } from 'react-redux'
import store from './redux/store'
import history from './redux/history'

import './styles/0_normalize.css'
import './styles/1_typography.css'
import App from './containers/App'

const rootEl = document.getElementById('root')

// Enable redux with the router
const RouterSetup = ({ App, store }) => {
  return(
    <Provider store={store}>
      <Router history={history}>
        <Route component={App} />
      </Router>
    </Provider>
  )
}

// Test if components should re-render - disable if console is going crazy
// if (process.env.NODE_ENV !== 'production') {
//   const {whyDidYouUpdate} = require('why-did-you-update')
//   whyDidYouUpdate(React)
// }

// Render React in public/index.html
ReactDOM.render(<RouterSetup App={App} store={store} />, rootEl)

// Enable hot reloading for deveopment
if (module.hot) {
  module.hot.accept('./containers/App', () => {
    const NextApp = require('./containers/App').default
    ReactDOM.render(<RouterSetup App={NextApp} store={store} />, rootEl)
  })
}

registerServiceWorker()
