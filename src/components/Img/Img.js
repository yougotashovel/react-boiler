import React, { PureComponent } from 'react'
import * as css from 'styles'
import styled from 'react-emotion'
import propStyles from 'prop-styles'

import Loading from 'components/Loading/Loading'
import Waypoint from 'react-waypoint'


const ImgWrapper = styled('div')({
  width: '100%',
  height: '400px',
  position: 'relative',
  overflow: 'hidden',
  backgroundColor: css.color.xlightgray,
  transition: css.transition.norm,
},({height, heightS, width})=>({
  height: height || '400px',
  width: width || '100%',
  [css.breakpoint.s_down]:{
    height: heightS ? `${heightS || '300px'} !important` : null,
  },
  transparent: {
    backgroundColor: 'transparent !important',
  }
}))

const ImgBackground = styled('div')({
  width: '100%',
  height: 'inherit',
  backgroundSize: 'cover',
  backgroundPosition: 'center center',
  transition: css.transition.norm,
},({height, heightS, src})=>({
  backgroundImage: `url(${src})`,
  height: height || '400px',
  [css.breakpoint.s_down]:{
    height: heightS ? `${heightS || '300px'} !important` : null,
  }
}),propStyles({
  grayscale: {
    filter: 'grayscale(1)',
  },
}))

// const ImgStyle = styled('img')({
//   width: '100%',
//   maxWidth: '100%',
//   opacity: '0 !important',
// },propStyles({
//   // grayscale: {
//   //   filter: 'grayscale(1)',
//   // },
// }))

class Img extends PureComponent {

  constructor() {
    super()
    this.state = {
      loaded: false,
      failed: false,
    }
  }

  loadSrc = () => {
    let downloadingImage = new Image()
    downloadingImage.onload = () =>  this.setState({loaded: true, failed: false})
    downloadingImage.onerror = () => this.setState({failed: true, loaded: false})
    downloadingImage.src = this.props.src;
    return downloadingImage
  }

  onEnter = () => {
    if (!this.state.loaded && !this.state.failed) {
      this.timer = setTimeout(()=>{
        this.loadSrc()
      }, this.props.timer || 350)
    }
  }

  onLeave = () => {
    this.timer && clearTimeout(this.timer)
  }

  tryAgain = () => {
    if (this.props.retry && this.state.failed) {
      this.setState({loaded: false, failed: false})
      this.loadSrc()
    }
  }

  render() {
    return(
      <Waypoint onEnter={this.onEnter} onLeave={this.onLeave}>
        <ImgWrapper
          width={this.props.width}
          height={this.props.height}
          heightS={this.props.heightS}
          onClick={this.tryAgain}
          transparent={this.props.transparent}
          style={this.props.style}
          >
          {this.props.noloading ?
            <ImgBackground
              grayscale={this.props.grayscale}
              height={this.props.height}
              heightS={this.props.heightS}
              src={this.props.src}
              alt={this.props.alt}
              style={this.props.style}
            />
            :
            <Loading
              medium
              loaded={this.state.loaded}
              error={this.state.failed}
              errormessage="Image not found"
              autoheight
              nospinner={this.props.nospinner}
              >
              <ImgBackground
                grayscale={this.props.grayscale}
                height={this.props.height}
                heightS={this.props.heightS}
                src={this.props.src}
                alt={this.props.alt}
                >
                {/* <ImgStyle {...this.props}/> */}
              </ImgBackground>
            </Loading>
          }
        </ImgWrapper>
      </Waypoint>
    )
  }
}


export default Img
