import React, { Component } from 'react'
import * as css from 'styles'
import styled from 'react-emotion'
import propStyles from 'prop-styles'

import InputWrapper from './InputWrapper'

// Dumb components
const Input = styled('input')({
  width: '90% !important',
  maxWidth: '90% !important',
  height: '100%',
  lineHeight: 'inherit',
  display: 'inline-block',
  border: 0,
  textAlign: 'right',
  padding: `0 ${css.space.s}`,

  borderBottomRightRadius: `0 !important`,
  borderTopRightRadius: `0 !important`,

  // States
  // ':hover':{}
  ':focus':{
    outline: 0,
  },

  // Media Queries
  [css.breakpoint.s_down]:{
    width: '80%',
    maxWidth: '80% !important',
  },
})

const IncrementWrapper = styled('div')({
  overflow: 'hidden',
  width: '10%',
  height: 'inherit',
  maxHeight: '100%',
  display: 'block',
  float: 'right',
  border: `${css.border.form} ${css.color.gray}`,
  borderLeft: `0 !important`,
  borderBottomRightRadius: `${css.border.radius} !important`,
  borderTopRightRadius: `${css.border.radius} !important`,

  // Media Queries
  [css.breakpoint.s_down]:{
    width: '20%',
  },
})

const Increment = styled('div')({
  cursor: 'pointer',
  userSelect: 'none',
  overflow: 'hidden',
  width: '100%',
  height: '50%',
  display: 'block',

  border: 0,
  borderBottom: `${css.border.form} ${css.color.gray}`,
  color: css.color.gray,
  backgroundColor: 'rgba(0,0,0,0.05)',

  textAlign: 'center',
  fontSize: '1.2em',
  lineHeight: '1.2em',

  // States
  ':hover':{
    color: css.color.black,
  },
  ':active':{
    backgroundColor: css.color.lightgray,
  },
  ':focus':{
    outline: 0,
  },

  // Media Queries
  [css.breakpoint.s_down]:{
    fontSize: '1em',
    lineHeight: '1em',
  },
},propStyles({
  noborder: {
    borderBottom: 0,
  }
}))

class InputNumber extends Component {

  constructor() {
    super()
    this.state = {
      value: 0,
    }

    this.input = this.input.bind(this)
  }

  componentWillMount() {
    this.setState({ value: this.props.value || this.props.min || 0 })
  }

  input = (e) => {
    let value = parseFloat(e.target.value)
    let max = this.props.max
    let min = this.props.min
    this.setState({ value: (value <= min) ? min : (value >= max) ? max : value })
  }

  add = () => {
    let increment = parseFloat(this.state.value) + 1
    let increment_max = (this.props.max >= increment) ? increment : this.props.max
    this.setState({ value: this.props.max ? increment_max : increment })
  }

  subtract = () => {
    let decrement = parseFloat(this.state.value) - 1
    let decrement_min = (this.props.min <= decrement) ? decrement : this.props.min
    this.setState({ value: this.props.min ? decrement_min : decrement })
  }

  render() {
    return (
      <InputWrapper>
        <Input
          {...this.props}
          type="number"
          pattern={this.props.pattern || `[0-9]`} // pattern="\d*"
          value={this.state.value}
          onChange={this.input}
          max={this.props.max}
          min={this.props.min}
        />
        <IncrementWrapper>
          <Increment onClick={this.add}>+</Increment>
          <Increment onClick={this.subtract} noborder>–</Increment>
        </IncrementWrapper>
      </InputWrapper>
    )
  }
}

export default InputNumber
