import React, { Component } from 'react'
import * as css from 'styles'
import styled from 'react-emotion'
import propStyles from 'prop-styles'

// Import Components and Containers
import FieldWrapper from './FieldWrapper'
import Label from './FieldLabel'
import Message from './FieldMessage'

import InputWrapper from './InputWrapper'
import InputNumber from './InputNumber'
import InputSelect from './InputSelect'
import { RadioButton, CheckBox } from './InputCheck'
import TextArea from './InputTextArea'
import File from './InputFile'


// Styles ONLY used in this component
export const style = {
  transition: css.transition.norm,
  overflow: 'hidden',
  width: '100%',
  // minWidth: '100%',
  // maxWidth: '100%',
  height: css.input.height,

  margin: 0,
  padding: `0 ${css.space.xs}`,

  backgroundColor: css.color.white,
  color: css.color.black,
  fontSize: css.font.l,
  borderRadius: css.border.radius,
  border: 0,
  borderBottom: `${css.border.form} ${css.color.gray}`,

  '&::placeholder':{
    transition: css.transition.fast,
  },

  // States
  ':hover': {
    // borderColor: css.color.black,
    '&::placeholder':{
      color: css.color.lightgray,
    }
  },
  ':focus': {
    outline: 0,
    borderColor: css.color.primary,
    '&::placeholder':{
      color: css.color.lightgray,
    }
  },
  // ':active': {
  //   borderColor: css.color.black,
  // },
  // '&:placeholder-shown': {
  //   color: css.color.lightgray
  // },

  // Breakpoints
  [css.breakpoint.s_down]: {
    fontSize: css.font.m,
    height: css.input.s_height,
  },
}

const error = {
  borderColor: css.color.red,
  ':hover': {
    borderColor: css.color.red,
  },
}

const success = {
  borderColor: css.color.green,
  ':hover': {
    borderColor: css.color.green,
  },
}

const inline = {
  width: 'auto'
}

const label = {
  // borderTopLeftRadius: css.border.radius,
}

const rounded = {
  borderRadius: css.font.l,
}


class InputSwitcher extends Component {
  renderLabel = () => this.props.label && <Label htmlFor={this.props.id}>{this.props.label}</Label>

  switchType = (type) => {
    switch(type){
      // Special input types with extra HTML
      case 'textarea': return(
        <div>
          { this.renderLabel() }
          <TextArea {...this.props} {...this.props.input} />
        </div>
      )
      case 'select': return(
        <div>
          { this.renderLabel() }
          <InputSelect {...this.props} {...this.props.input} />
        </div>
      )
      case 'number': return(
        <div>
          { this.renderLabel() }
          <InputNumber {...this.props} {...this.props.input} />
        </div>
      )
      case 'checkbox': return(
        <div>
          <CheckBox {...this.props} {...this.props.input} />
          { this.renderLabel() }
        </div>
      )
      case 'radio': return(
        <div>
          <RadioButton {...this.props} {...this.props.input} />
          { this.renderLabel() }
        </div>
      )
      case 'file': return(
        <div>
          { this.renderLabel() }
          <InputWrapper>
            <File {...this.props} {...this.props.input} />
          </InputWrapper>
        </div>
      )
      // Everything else
      default: return(
        <div>
          { this.renderLabel() }
          <InputWrapper>
            <input {...this.props} {...this.props.input} />
          </InputWrapper>
        </div>
      )
    }
  }

  render() {
    return this.switchType(this.props.type)
  }
}

const InputWithStyles = styled(InputSwitcher)(
  style,
  propStyles({
    error,
    success,
    inline,
    label,
    rounded,
  })
)

class Input extends Component {

  render() {
    const { errors, success, input } = this.props
    const hasError = errors && errors[input ? input.name : this.props.name]
    const hasSuccess = success && success[input ? input.name : this.props.name]
    const filteredProps = { ...this.props, errors: null, success: null }

    return (
      <FieldWrapper>
        <InputWithStyles {...filteredProps} error={hasError ? 1 : 0} success={hasSuccess ? 1 : 0} />
        { hasError && <Message error>{hasError}</Message> }
        { hasSuccess && <Message success>{hasSuccess}</Message> }
      </FieldWrapper>
    )
  }
}

export default Input
