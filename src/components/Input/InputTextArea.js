import * as css from 'styles'
import styled from 'react-emotion'

const TextArea = styled('textarea')({
  paddingTop: `${css.space.xs} !important`,
  height: 'auto !important',
  minHeight: '5em',
})

export default TextArea
