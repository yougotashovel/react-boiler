import * as css from 'styles'
import styled from 'react-emotion'
// import propStyles from 'prop-styles'

const FieldLabel = styled('label')({
  color: css.color.gray,
})

export default FieldLabel
