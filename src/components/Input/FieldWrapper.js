import * as css from 'styles'
import styled from 'react-emotion'
import propStyles from 'prop-styles'

const FieldWrapper = styled('div')({
  fontFamily: css.font.main,
  display: 'inline-block',
  width: '100%',
  position: 'relative',

  margin: css.spacing(),
  marginTop: 0,
  marginLeft: 0,
},propStyles({
  inline: {width: 'auto'}
}))

export default FieldWrapper
