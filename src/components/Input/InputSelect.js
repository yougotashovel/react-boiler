import React from 'react'
import * as css from 'styles'
import styled from 'react-emotion'
// import propStyles from 'prop-styles'

import InputWrapper from './InputWrapper'

const style = {
  width: '100%',
  height: '100%',
  paddingLeft: 0, //`${css.space.s} !important`,
  paddingRight: 0, //`${css.space.s} !important`,
  // marginLeft: `-${css.space.s} !important`,
  // marginRight: `-${css.space.s} !important`,
  border: 0,
  background: 'transparent !important',
  appearance: 'none',
  position: 'relative',
}

const SelectWrapper = styled('div')({
  width: '100%',
  height: '100%',
  '::after':{
    transition: css.transition.fast,
    content: "▼", // this is a down triangle ▼  '\25B2'
    // pointerEvents: 'none',
    position: 'absolute',
    zIndex: 2,
    right: css.space.s,
    top: css.space.s,
    fontSize: css.font.l,
    height: '100%',
    lineHeight: '100%',
    color: css.color.gray,

    [css.breakpoint.s_down]:{
      right: css.space.s,
      top: css.space.s,
      fontSize: css.font.m,
    },
  },

  ':hover': {
    '::after':{
      color: css.color.black,
    },
  },
})

const Select = (props) => (
  <InputWrapper>
    <SelectWrapper>
    <select {...props}>{props.children}</select>
    </SelectWrapper>
  </InputWrapper>
)

export default styled(Select)(style)
