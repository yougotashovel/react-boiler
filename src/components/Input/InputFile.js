import * as css from 'styles'
import styled from 'react-emotion'

const File = styled('input')({
  paddingTop: `${css.space.xs} !important`,
  lineHeight: '1em !important',
})

export default File
