### Regular Input

```jsx
<div>
  <Input
    type="text"
    placeholder="Text input"
    name="text"
  />

  <Input
    type="email"
    placeholder="Email input"
    name="email"
  />

  <Input
    type="textarea"
    placeholder="Text Area"
    name="text_area"
  />
</div>
```

### Checkbox

Must have `id` to be selectable.

```jsx
<div>
  <Input
    type="checkbox"
    label="Checkbox 1"
    name="checkbox_1"
    id="checkbox_1"
  />

  <Input
    type="checkbox"
    label="Checkbox 2"
    name="checkbox_2"
    id="checkbox_2"
  />
</div>
```

### Radio Group

Must have `id` to be selectable.

```jsx
<div>
  <Input
    type="radio"
    label="Radio 1"
    name="radiogroup"
    id="radio_1"
  />

  <Input
    type="radio"
    label="Radio 2"
    name="radiogroup"
    id="radio_2"
  />

  <Input
    type="radio"
    label="Radio 3"
    name="radiogroup"
    id="radio_3"
  />
</div>
```

### Input States

```jsx
<div>
  <Input
    label="Input Label"
    type="text"
    placeholder="Text input"
    name="text"
  />

  <Input
    label="Input Label"
    type="text"
    placeholder="Error input"
    name="text"
    errors={{text: 'Text is invalid'}}
  />

  <Input
    label="Input Label"
    type="text"
    placeholder="Success input"
    name="text"
    success={{text: 'Text is valid'}}
  />

</div>
```
