import React from 'react'
import * as css from 'styles'

export default (props) => (
    <ul>
        {props.children}
    </ul>
)