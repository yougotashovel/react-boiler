import React from 'react'
import * as css from 'styles'
import styled from 'react-emotion'
import propStyles from 'prop-styles'

import View from 'components/View/View'
import FadeIn from 'components/Animated/FadeIn'
import Spinner from './Spinner'

const LoadingWrapper = styled('div')({
  // padding: css.space.xl,
  height: '100%', // height: '400px',

  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',

  textAlign: 'center',
  fontSize: css.font.r,
  color: css.color.primary,
  overflow: 'hidden',
},
  ({ height }) => ({
    height: height || '100%',
  }),
  propStyles({
    inline: { display: 'inline-block',}
  })
)

const Message = styled(View)({
  marginTop: css.space.s,
})


const Loading = (props) => {
  if (props.loaded) { // && props.children
    return (
      <FadeIn>
        {props.children}
      </FadeIn>
    )
  } else if (props.error) {
    return (
      <LoadingWrapper
        height={props.height}
        inline={props.inline}
        >
        <FadeIn>
          <Message color={css.color.gray}>{props.errormessage || 'Failed to load'}</Message>
        </FadeIn>
      </LoadingWrapper>
    )
  } else if (props.loading===false) {
    return null
  } else {
    return (
      <LoadingWrapper
        height={props.height}
        inline={props.inline}
        >
        <FadeIn>
          {!props.nospinner &&
            <Spinner
              small={props.small}
              medium={props.medium}
            />
          }
          {props.message &&
            <FadeIn delay="200ms">
              <Message>{props.message}</Message>
            </FadeIn>
          }
        </FadeIn>
      </LoadingWrapper>
    )
  }
}

export default Loading
