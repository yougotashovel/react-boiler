import * as css from 'styles'
import styled from 'react-emotion'
import propStyles from 'prop-styles'
import { spin } from 'styles/animations'

const size = '7em'
const size_small = '2.4em'
const size_medium = '5em'
const border = '3px'
const border_small = '2px'
const border_medium = '3px'
const track_color = 'rgba(0,0,0, 0.1)'
const main_color = css.color.primary

export default styled('div')({
  // margin: '20px auto',
  margin: 0,
  fontSize: '10px',
  position: 'relative',
  textIndent: '-9999em',
  borderTop: `${border} solid ${track_color}`,
  borderRight: `${border} solid ${track_color}`,
  borderBottom: `${border} solid ${track_color}`,
  borderLeft: `${border} solid ${main_color}`,
  transform: 'translateZ(0)',
  animation: `${spin} 1.1s infinite linear`,

  borderRadius: '50%',
  width: size,
  height: size,

  '&:after': {
    borderRadius: '50%',
    width: size,
    height: size,
  },
},propStyles({
  small: {
    borderTop: `${border_small} solid ${track_color}`,
    borderRight: `${border_small} solid ${track_color}`,
    borderBottom: `${border_small} solid ${track_color}`,
    borderLeft: `${border_small} solid ${main_color}`,

    width: size_small,
    height: size_small,

    '&:after': {
      width: size_small,
      height: size_small,
    },
  },
  medium: {
    borderTop: `${border_medium} solid ${track_color}`,
    borderRight: `${border_medium} solid ${track_color}`,
    borderBottom: `${border_medium} solid ${track_color}`,
    borderLeft: `${border_medium} solid ${main_color}`,

    width: size_medium,
    height: size_medium,

    '&:after': {
      width: size_medium,
      height: size_medium,
    },
  }
}))
