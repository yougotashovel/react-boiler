Loading objects by default fit to container size.  Set a height to prevent this.

```jsx
<div>
  <div style={{height: '300px'}}>
  <Loading loaded={false} autoheight nopadding/>
  </div>
</div>
```
