// import React from 'react'
import * as css from 'styles'
import styled from 'react-emotion'
// import propStyles from 'prop-styles'

const tablePadding = css.space.xs
const style = {
  width: '100%',
  padding: `0`,
  fontSize: css.font.r,

  // Header
  '> thead': {
    fontWeight: css.font.bold,
    // Row
    '> tr': {
      // Cell
      '> td': {
        padding: tablePadding,
      },
    },
  },

  // Body
  '> tbody': {
    // Row
    '> tr': {
      borderBottom: `2px solid ${css.color.lightgray}`,
      '&:nth-child(even)':{
        // backgroundColor: 'hsla(0,0%,95%,1)',
      },
      '&:nth-child(odd)':{
        // backgroundColor: 'hsla(0,0%,90%,1)',
      },
      '&:last-child': {
        borderBottom: 0,
      },
      // Cell
      '> td': {
        padding: tablePadding,
        '&:last-child': {
          textAlign: 'right',
        },
      },
    },
  },

  // Footer
  '> tfoot':{
    // Row
    '> tr': {
      // Cell
      '> td': {
        padding: tablePadding,

        '&:last-child':{
          textAlign: 'right',
        },
      },
    },
  },
}


export default styled('table')(
  style,
)
