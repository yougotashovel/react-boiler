import * as css from 'styles';
import styled from 'react-emotion';
import propStyles from 'prop-styles';
import { fadeUp } from '../../styles/animations';
import posed from 'react-pose';
import { tween, easing } from 'popmotion';

const Overlay = styled('div')({
  position: 'fixed',
  zIndex: css.z.modal + 1,

  margin: '0 auto',
  maxWidth: '400px',
  width: '100%',
  minHeight: '300px', // make these the same value - for loading to appear in center
  // height: '320px', // make these the same value - for loading to appear in center

  padding: `${css.space.xl}`,
  borderRadius: css.border.radius,
  backgroundColor: css.color.white,

  // transform: 'translateZ(0)',
  // transition: css.transition.slow,

  // animation: `${fadeUp} ${css.speed[4]}ms ease both running forwards`,

}, propStyles({
  closed: {
    opacity: 0,
    transform: 'translateY(30px)',
    // animation: `${fadeUp} ${css.speed[4]}ms ease reverse both running`,
  },
  autoheight: {
    minHeight: 'auto',
    height: 'auto',
  }
}))

const open = {
  opacity: 1,
  y: 0,
  // transform: 'translateY(0px)',
}

const closed = {
  opacity: 0,
  y: 100,
  // transform: 'translateY(30px)',
}

const confirmed = {
  opacity: 0,
  y: -400,
  // transform: 'translateY(-30px)',
}

export default posed(Overlay)({
  initialPose: 'closed',
  open: {
    ...open,
    transition: (props) => tween({ ...props,
      // from: closed,
      // to: open,
    })
  },
  closed: {
    ...closed,
    transition: (props) => tween({ ...props, ease: easing.easeInOut,
      // from: open,
      // to: closed,
    })
  },
  confirmed: {
    ...confirmed,
    transition: (props) => tween({ ...props, // ease: easing.easeInOut,
      duration: 1000,
      // from: open,
      // to: confirmed,
    })
  },
})
