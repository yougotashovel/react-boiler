import React from 'react';
import withKeys from 'components/withKeys';
import { connect } from 'react-redux';

import Screen from './Screen';
import Overlay from './Overlay';


// const Modal = ({ open, close, children }) => (
class Modal extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      open: props.open,
      pose: 'closed',
    }
  }

  componentDidMount() {
    this.props.keymap({
      27: () => this.close(), // escape

      // Testing controls - delete
      49: () => this.open(), // 1
      50: () => this.confirmed(), // 2
      51: () => this.toggle(), // 3
    })

    this.props.name && this.props.register({ name: this.props.name })
  }

  componentWillUnmount() {
    this.props.name && this.props.unregister({ name: this.props.name })
  }

  close = (event) => {
    if (event && (event.target !== event.currentTarget)) return
    this.props.close && this.props.close(event)
    // this.setState({open: false, pose: 'closed' })
    this.props.closeModal({name: this.props.name})
  }

  open = (event) => {
    // this.setState({open: true, pose: 'open'})
    this.props.openModal({name: this.props.name})
  }

  confirmed = (event) => {
    // this.setState({open: false, pose: 'confirmed'})
    this.props.confirmModal({name: this.props.name})
  }

  toggle = (event) => {
    // this.setState({ open: !this.state.open, pose: this.state.open ? 'closed' : 'open' })
    this.props.open ? this.close() : this.open()
  }

  render() {
    const { close, children, autoheight } = this.props
    return (
      <Screen
        onClick={this.close}
        closed={!this.props.open}
        >
        <Overlay
          // closed={!this.state.open}
          pose={this.props.pose}
          autoheight={autoheight}
          >
          {this.props.open && children}
          {/* {children} */}
        </Overlay>
      </Screen>
    )
  }
}
// )

const mapState = (state, props) => ({
  open: state.modals[props.name] ? state.modals[props.name].open : false,
  pose: state.modals[props.name] ? state.modals[props.name].pose : 'closed',
})

const mapDispatch = dispatch => ({
  register: dispatch.modals.register,
  unregister: dispatch.modals.unregister,
  closeModal: dispatch.modals.close,
  openModal: dispatch.modals.open,
  confirmModal: dispatch.modals.confirm,
})

export default connect(mapState, mapDispatch)(withKeys(Modal))
