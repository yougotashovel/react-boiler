Basic box element. Use like a <div>

```jsx static
<View>
  Basic block element
</View>

<View inline>
  Basic inline element
</View>
```
