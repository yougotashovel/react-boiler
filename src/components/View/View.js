// import React from 'react';
import * as css from 'styles';
// import styled from 'react-emotion';
import styled from 'react-emotion';
import propStyles from 'prop-styles';

const View = styled('div')({
  display: 'block',
},
({ margin, padding, height, background, color, float, opacity }) => ({
  margin: margin || 'default',
  padding: padding || 'default',
  height: height || 'default',
  background: background || 'default',
  color: color || 'inherit',
  float: float || 'none',
  opacity: opacity || 1,
}),
propStyles({
  inline: {display: 'inline'},
  inlineBlock: {display: 'inline-block'},
  transition: {transition: css.transition.norm },
}))

export default View;
