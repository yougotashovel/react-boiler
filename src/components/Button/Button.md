### Regular Buttons

```js
<div>
  <Button>Regular</Button>
  <Button outlined>Outlined</Button>
  <Button disabled>Disabled</Button>
  <Button text>Text</Button>
</div>
```

### Small Buttons

```js
<div>
  <Button small>Small Regular</Button>
  <Button small outlined>Small Outlined</Button>
  <Button small disabled>Small Disabled</Button>
  <Button small text>Small Text</Button>
</div>
```
