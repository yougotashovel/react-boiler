// import React from 'react';
import * as css from 'styles'
import styled from 'react-emotion';
import propStyles from 'prop-styles';

const Button = styled('button')({
  transition: css.transition.norm,
  // margin: css.spacing(),
  // marginTop: 0,
  // marginLeft: 0,
  marginRight: css.space.s,
  padding: `${css.space.xs} ${css.space.s}`,

  backgroundColor: css.color.primary,
  color: css.color.white,
  fontSize: css.font.l,
  borderRadius: css.border.radius,
  border: `${css.border.form} ${css.color.primary}`,

  // States
  '&:hover': {
    backgroundColor: `${css.color.primarydark}`,
    borderColor: css.color.primarydark,
    color: css.color.white,
  },
  '&:focus': {
    outline: 0,
    backgroundColor: css.color.primary,
    borderColor: css.color.primarydark,
  },
  '&:active': {
    backgroundColor: `${css.color.black}`,
    borderColor: css.color.black,
  },

  // Breakpoints
  [css.breakpoint.s_down]: {
    fontSize: css.font.m
  },
},propStyles({

  active: {
    backgroundColor: `${css.color.black} !important`,
    borderColor: `${css.color.black} !important`,
  },

  fit: {
    width: '100%',
  },

  outlined: {
    border: `${css.border.form} ${css.color.black}`,
    backgroundColor: 'transparent', //css.color.white,
    color: css.color.black,

    // States
    '&:hover, &:focus, &:active': {
      backgroundColor: `${css.color.white} !important`,
      borderColor: css.color.primary,
      color: css.color.primary,
    }
  },

  text: {
    borderColor: 'transparent',
    backgroundColor: 'transparent',
    color: css.color.primary,

    // States
    '&:hover, &:focus, &:active': {
      backgroundColor: 'transparent !important',
      borderColor: 'transparent !important',
      color: `${css.color.primarydark} !important`,
    },

    '&:disabled': {
      backgroundColor: 'transparent !important',
      borderColor: 'transparent !important',
      color: `${css.color.primarydark} !important`,
      opacity: 0.2,
    },
  },

  disabled: {
    backgroundColor: css.color.gray,
    borderColor: css.color.gray,

    // States
    '&:hover, &:focus, &:active': {
      backgroundColor: `${css.color.gray} !important`,
      borderColor: `${css.color.gray} !important`,
    }
  },

  small: {
    padding: `${css.space.xs} ${css.space.s}`,
    fontSize: css.font.r,
    // margin: 0,
  },

  xsmall: {
    padding: `${css.space.xxs} ${css.space.xs}`,
    fontSize: css.font.r,
    // margin: 0,
    [css.breakpoint.s_down]:{
      fontSize: css.font.r,
    }
  },

  nomargin: {
    margin: 0,
  },
  nopadding: {
    padding: 0,
  }
}))

export default Button
