import styled from 'react-emotion'
import * as css from 'styles'
import propStyles from 'prop-styles'

import { fadeIn, fadeUp, fadeDown, fadeLeft, fadeRight } from 'styles/animations'

export default styled('div')({
  opacity: 0,
  animation: `${fadeIn} ${css.speed[3]}ms linear forwards`,
}, ({delay}) => ({
  animationDelay: delay || 0,
}),propStyles({
  up: { animationName: fadeUp, animationTimingFunction: 'ease' },
  down: { animationName: fadeDown, animationTimingFunction: 'ease' },
  left: { animationName: fadeLeft, animationTimingFunction: 'ease' },
  right: { animationName: fadeRight, animationTimingFunction: 'ease' },
}))
