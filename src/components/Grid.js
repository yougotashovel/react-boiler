// import React from 'react'
import * as css from 'styles'
import styled from 'react-emotion'
import propStyles from 'prop-styles'

// const grid_columns = 12
const default_gutter = css.space.r
const widths = {
  u12: '100% !important',
  u11: '91.666666 !important',
  u10: '83.333333 !important',
  u9: '75% !important',
  u8: '66.666666% !important',
  u7: '58.333333% !important',
  u6: '50% !important',
  u5: '41.666666% !important',
  u4: '33.333333% !important',
  u3: '25% !important',
  u2: '16.666666% !important',
  u1: '8.333333% !important',
}
const child_widths = {}
Object.keys(widths).map((width,i)=>{ return child_widths[width] = {'> div':{ flex: `0 0 ${widths[width]}` }} })
const column_sizes = {
  fit:    {'> div':{ flex: 1 }},
  full:   {'> div':{ flex: '0 0 100%' }},
  ...child_widths
}


// Exportable components
export const Grid = styled('div')({
  display: 'flex',
  flexWrap: 'wrap',
  listStyle: 'none',
  margin: 0,
  padding: 0,

},
// Variable gutters
({ gutter = 0 }) => ({
  margin: `-${gutter} 0 0 -${gutter}`,
  '> div':{ padding: `${gutter} 0 0 ${gutter}` },
}),
propStyles({
  // Default gutters
  gutters: {
    margin: `-${default_gutter} 0 0 -${default_gutter}`,
    '> div':{ padding: `${default_gutter} 0 0 ${default_gutter}` },
  },

  // Row-level alignment
  top:    { alignItems: 'flex-start' },
  bottom: { alignItems: 'flex-end' },
  middle: { alignItems: 'center' },
  center: { justifyContent: 'center' },

  // Row-level props - applies to every Col
  ...column_sizes,
}))


// const cellBreakpoint = (point) => ({
//   [css.breakpoint[point+'_only']]: {
//     display: (point==='hide') ? 'none !important' : '',
//     width: widths[point],
//     flex: point && 'none !important',
//   }
// })

// function cellBreakpoint(point) {
//   return {
//     [css.breakpoint[point+'_only']]: {
//       display: (point==='hide') ? 'none !important' : '',
//       width: widths[point],
//       flex: point && 'none !important',
//     }
//   }
// }

export const Col = styled('div')({
  flex: 1,
},
// Cell-level breakpoints
// ({ xs }) => cellBreakpoint(xs),
// ({ s })  => cellBreakpoint(s),
// ({ m })  => cellBreakpoint(m),
// ({ l })  => cellBreakpoint(l),
// ({ xl }) => cellBreakpoint(xl),
({ xs }) => ({
  [css.breakpoint.xs_only]:{
    display: (xs==='hide') ? 'none !important' : '',
    width: widths[xs],
    flex: xs && 'none !important',
  }
}),
({ s })  => ({
  [css.breakpoint.s_only]: {
    display: (s==='hide') ? 'none !important' : '',
    width: widths[s],
    flex: s && 'none !important',
  }
}),
({ m })  => ({
  [css.breakpoint.m_only]: {
    display: (m==='hide') ? 'none !important' : '',
    width: widths[m],
    flex: m && 'none !important',
  }
}),
({ l })  => ({
  [css.breakpoint.l_only]: {
    display: (l==='hide') ? 'none !important' : '',
    width: widths[l],
    flex: l && 'none !important',
  }
}),
({ xl }) => ({
  [css.breakpoint.xl_only]:{
    display: (xl==='hide') ? 'none !important' : '',
    width: widths[xl],
    flex: xl && 'none !important'
  }
}),
propStyles({
  // Cell-level alignment
  top:    { alignSelf: 'flex-start' },
  bottom: { alignSelf: 'flex-end' },
  middle: { alignSelf: 'center' },

  tac: { textAlign: 'center' },
  tal: { textAlign: 'left' },
  tar: { textAlign: 'right' },

  tacS: { [css.breakpoint.s_down]:{textAlign: 'center'} },
  talS: { [css.breakpoint.s_down]:{textAlign: 'left'} },
  tarS: { [css.breakpoint.s_down]:{textAlign: 'right'} },

  tacM: { [css.breakpoint.m_down]:{textAlign: 'center'} },
  talM: { [css.breakpoint.m_down]:{textAlign: 'left'} },
  tarM: { [css.breakpoint.m_down]:{textAlign: 'right'} },

  // Cell BorderGrid styles
  border: {
    borderBottom: `${css.border.solid} ${css.color.primary}`, // doesnt work, because of flexgrid?
  },
  left:   {

  },
  right:  {
    borderLeft: `${css.border.solid} ${css.color.primary}`,
    [css.breakpoint.s_down]:{ border: 0 }
  },
  // borderTopS: {
  //   borderTop: `${css.border.solid}`,
  // },
  dashed: {
    borderStyle: `${css.border.dashed} !important`,
    borderColor: `${css.color.primary}`,
  },
  dashedwhite: {
    borderStyle: `${css.border.dashed} !important`,
    borderColor: `${css.color.white}`,
  },

  // Cell Padding
  padding: {
    padding: css.space.xl,

    [css.breakpoint.s_down]: {
      padding: css.space.r,
    },
  },
  inline: {
    display: 'inline',
  }
}))

export const Padding = styled('div')({
  padding: `${css.space.r} ${css.space.l}`,

  [css.breakpoint.s_down]: {
    padding: css.space.s,
  },
}, ({size})=>({
  padding: size,
}),propStyles({
  nopadding: {
    padding: '0 !important',
  }
}))

export const Left = styled('div')({
  height: '100%',
})

export const Right = styled('div')({
  height: '100%',
  borderLeft: `${css.border.solid} ${css.color.primary}`,

  // Breakpoints
  [css.breakpoint.s_down]:{
    border: 0,
  }
})

export const Border = styled('div')({
  borderBottom: `${css.border.solid} ${css.color.primary}`
})
