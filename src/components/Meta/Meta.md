Allows you to set <meta> tags for current page.

```jsx static
<Meta
  title="Home"
  description="Description of page"
/>
```
