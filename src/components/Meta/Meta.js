import React, { PureComponent } from 'react'
import { meta } from 'settings'
import Helmet from 'react-helmet'

// import ReactGA from 'react-ga'
// ReactGA.initialize(settings.analyticsKey)

class Meta extends PureComponent {

  // componentDidMount() {
  //   ReactGA.pageview(window.location.pathname + window.location.search)
  // }

  render() {
    const { defaults, title, description, children } = this.props
    return (
      <Helmet>
        {defaults && <title>{meta.title}</title>}
        {title && <title>{title} {meta.separator} {meta.title}</title>}
        {description && <meta name="description" content={description} />}
        {children}
      </Helmet>
    )
  }
}

export default Meta
