import React from 'react';
// import styled from 'react-emotion';
import styled from 'react-emotion';
import propStyles from 'prop-styles';
import * as css from 'styles'

const blockStyles = propStyles({
  nospace: { margin: 0, padding: 0, },
  block: { display: 'block' },
  inline: { display: 'inline' },
  inlineBlock: { display: 'inline-block' },
  transition: { transition: css.transition.norm },
  italics: { fontStyle: 'italic' },
})

const varStyles = ({ color, float }) => ({
  color: color || 'default',
  float: float || 'none',
})

const headingStyle = {
  margin: 0,
  padding: 0,
  marginBottom: css.space.xxs,
}

const textElements = {
  span: styled('span')({color: 'black'}, varStyles, blockStyles),
  h1: styled('h1')({...headingStyle, fontSize: css.font.xxl, }, varStyles, blockStyles),
  h2: styled('h2')({...headingStyle, fontSize: css.font.xl, },  varStyles, blockStyles),
  h3: styled('h3')({...headingStyle, fontSize: css.font.l, },   varStyles, blockStyles),
  h4: styled('h4')({...headingStyle, fontSize: css.font.r, },   varStyles, blockStyles),
  h5: styled('h5')({...headingStyle, fontSize: css.font.s, },   varStyles, blockStyles),
  h6: styled('h6')({...headingStyle, fontSize: css.font.xs, },  varStyles, blockStyles),
}

const Text = (props) => {
  const Element = textElements[props.type || 'span']
  return <Element {...props} />
}

export default Text;
