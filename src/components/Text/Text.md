Basic box element. Use like a <div>

```jsx static
<Text>
  Basic block element
</Text>

<Text inline>
  Basic inline element
</Text>
```
