import React from 'react';
import Link, { NavLink } from 'redux-first-router-link';

// Abstracts Link, incase library is changed in the future. Only need to update this file.
// Projects can import Link from the usual /components/ folder.
// This component can be used as <Link> or <Link nav>

export default ({ nav, children, ...props }) => {
  if (nav) {
    return <NavLink {...props} exact={false}>{children}</NavLink>
  } else {
    return <Link {...props}>{children}</Link>
  }
}
