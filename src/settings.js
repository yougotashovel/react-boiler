// This file contains:
// 1. Set API base URL
// 2. Set API keys for 3rd party services e.g: Google Maps
// 3. Enable/disable JS animations and CSS transitions - helps with testing performance.

export default {
  // Animation / Transition settings
  animations: {
    on: true,
  },
  transitions: {
    on: true,
  },
}

export const meta = {
  title: 'App',
  separator: '-',
}
