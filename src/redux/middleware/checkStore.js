// import { dispatch } from '@rematch/core'

const checkStore = ({ getState, dispatch }) => next => action => {
  // Check if this is an API request with 'checkStore' middleware
  if (action.request && action.request.middleware && action.request.middleware.checkStore) {
    // If data is already loaded in the store, return, and let redux know
    if (action.request.middleware.checkStore(getState())) {
      // console.log(action.type,'data already in store');
      // dispatch({ type: action.type+'/from-store' })
      return
    }
  }

  next(action)
}

export default checkStore
