import { dispatch } from '@rematch/core';

// 1. Action type <-> url path.
// 2. :id is dynamic route parameter
// 3. Possible to add thunks to send another action on route loading.
// 4. Weird behaviour - rematch dispatch functions need to be awaited before called.
// 5. When getting data based on route - containers used on other routes will not show data.

export const routesMap =  {
  // 'ACTION_NAME': '/url-path-for-action',
  'route/home': { path: '/' },
  'route/counter': { path: '/counter' },
  'route/transitions': { path: '/transitions' },

  'route/posts': {
    path: '/posts',
    thunk: async (d, getState) => { await dispatch; // wait for rematch to load
      dispatch.posts.fetch({})
    }
  },

  'route/photos': {
    path: '/photos',
    thunk: async (d, getState) => { await dispatch; // wait for rematch to load
      dispatch.photos.fetch({})
    }
  },

  'route/photo': {
    path: '/photos/:id',
    thunk: async (d, getState) => { await dispatch; // wait for rematch to load
      dispatch.photo.fetch({id: getState().location.payload.id})
    }
  },

  'route/products': {
    path: '/products',
    thunk: async (d, getState) => { await dispatch; // wait for rematch to load
      dispatch.products.fetch({})
    }
  },

  'route/cart': {
    path: '/cart',
    thunk: async (d, getState) => { await dispatch; // wait for rematch to load
      dispatch.products.fetch({})
    }
  },

  'route/checkout': {
    path: '/checkout',
    thunk: async (d, getState) => { await dispatch; // wait for rematch to load
      dispatch.products.fetch({})
    }
  },
}
