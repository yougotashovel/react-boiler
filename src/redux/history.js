// This is only abstracted because i can't access history within redux-middleware,
// normally it's automatically created by the BrowserRouter,
// import history to middleware/apiRequest.js
// and inject history into BrowserRouter on index.js

import createHistory from 'history/createBrowserHistory'
export default createHistory()
