import objDeleteProp from 'utils/objDeleteProp';
import objAddProp from 'utils/objAddProp';
import objDeleteAllProps from 'utils/objDeleteAllProps';

const initialState = () => ({
  message: null,
  items: {},
  itemQty: 0,
  totals: {
    subtotal: 0,
    tax: 0,
    total: 0,
  },
})

export const withCurrency = (price = 0, currency = '$') => {
  return `${currency}${Math.floor(price)}`
}

export const cartItemsCount = (items) => {
  // Takes items object, and adds the value for each key together.
  return Object.keys(items).reduce((total, item) => total+items[item], 0)
}

export const getLineTotal = (price, quantity) => {
  return parseFloat(price.toFixed(2)) * quantity
}

export const getLineTotals = (lineTotals) => {
  return lineTotals.reduce((subtotal, line)=> subtotal+line, 0)
}

export const getSubtotal = (items, products) => {
  return Object.keys(items).reduce((total, item) => total+products[item].price, 0)
}

export const getTax = (taxPercent, subtotal) => {
  return parseFloat(taxPercent.toFixed(2)) * parseFloat(subtotal)
}

export const getTotal = (totals) => {
  return totals.reduce((total, item) => total + parseFloat(item), 0)
}

export const lowest = (number, min = 1) => {
  return (number < min) ? min : number;
}

export const cart = {
  name: 'cart',
  state: initialState(),
  reducers: {
    empty: (state, payload) => ({...initialState() }),
    add: (state, payload) => ({...state,
      message: null,
      items: objAddProp(state.items, payload.id, payload.qty || 1) }),
    remove: (state, payload) => ({...state,
      message: null,
      items: objDeleteProp(state.items, payload.id) }),

    increase: (state, payload) => ({...state,
      message: null,
      items: objAddProp(state.items, payload.id, (state.items[payload.id] || 0) + 1) }),
    decrease: (state, payload) => ({...state,
      message: null,
      items: objAddProp(state.items, payload.id, lowest((state.items[payload.id] || 0) - 1)) }),

    updateTotals: (state, payload) => ({...state, totals: payload}),
    populate: (state, payload) => ({...state, items: payload.items, message: payload.message, }),
    // 'products/success': (state) => ({...state }), // listener - to trigger updateTotals for prepopulating
  },
  effects: {
    // itemsInCart: (payload, store) => itemsInCart(),
  }
}
