export const photo = {
  name: 'photo',
  request: {
    url: 'http://localhost:3004/posts/:id', //'https://jsonplaceholder.typicode.com/comments',
    middleware: {
      checkStore: store => store.location.payload.id === store.photo.data.id
    }
  },
  state: {
    data: {}, // photo needs an empty object
  },
}
