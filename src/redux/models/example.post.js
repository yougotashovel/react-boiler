export const post = {
  name: 'post',
  request: {
    url: 'https://jsonplaceholder.typicode.com/posts/:id',
  }
}
