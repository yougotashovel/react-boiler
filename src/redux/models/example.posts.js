import { makeVisibleData } from '../plugins/filters/filters.selectors'

export const posts = {
  name: 'posts',
  request: {
    url: 'https://jsonplaceholder.typicode.com/posts', //`/posts`,
    middleware: {
      checkStore: (store) => store.posts.data.length, // check if data is already in the store
    }
  },
  state: {
    data: [] // posts needs an empty array, so we override default 'data' here
  },
  filters: {
    search: (item, test) => `${item.title}`.toLowerCase().includes(test.toLowerCase())
  },
}

export const getVisibleData = makeVisibleData(posts.filters)
