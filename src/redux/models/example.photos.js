import { makeVisibleData } from '../plugins/filters/filters.selectors'

export const photos = {
  name: 'photos',
  request: {
    url: 'http://localhost:3004/posts', //'https://jsonplaceholder.typicode.com/comments',
    middleware: {
      checkStore: (store) => store.photos.data.length, // check if data is already in the store
    }
  },
  state: {
    data: [], // items needs an empty array, so we override default 'data' here
  },
  filters: {
    gender: (item, test) => item.gender.toLowerCase() === test.toLowerCase(),
    search: (item, test) => `${item.first_name}${item.last_name}`.toLowerCase().includes(test.toLowerCase())
  },
  urlQuery: true, // e.g: /photos?gender=male
}

export const getVisibleData = makeVisibleData(photos.filters)
