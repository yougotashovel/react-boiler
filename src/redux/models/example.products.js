export const products = {
  name: 'products',
  request: {
    url: 'http://localhost:3004/products', //'https://jsonplaceholder.typicode.com/comments',
    middleware: {
      checkStore: (store) => store.products.data.length, // check if data is already in the store
    }
  },
  state: {
    data: [], // items needs an empty array, so we override default 'data' here
  },
}
