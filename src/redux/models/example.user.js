const initialState = () => ({
  data: {
    id: null,
    email: null,
    first_name: null,
    last_name: null,
    shipping: {},
    billing: {},
    orders: [],
    preferences: {},
  }
})

export const user = {
  name: 'user',
  request: {
    url: 'http://localhost:3004/users?email=:email',
  },
  state: initialState(),
  reducers: {

  },
  effects: {
    hasAccount(payload, state) {},
    getOrders(payload, state) {},
    getPreferences(payload, state) {},
  }
}
