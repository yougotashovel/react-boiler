import deleteProp from 'utils/objDeleteProp';

const closed = () => ({
  open: false,
  pose: 'closed',
})

const open = () => ({
  open: true,
  pose: 'open',
})

const confirmed = () => ({
  open: false,
  pose: 'confirmed',
})

const newState = (open, pose) => ({ open, pose })

const closeOtherModals = (state) => {
  const modals = Object.keys(state)

}

export const modals = {
  name: 'modals',
  state: {}, // blank object
  reducers: {
    register: (state, payload) => ({...state, [payload.name]: closed() }),
    unregister: (state, payload) => ({ ...deleteProp(state, payload.name) }),

    close: (state, payload) => ({...state, [payload.name]: closed() }),
    open: (state, payload) => ({...state, [payload.name]: open() }),
    confirm: (state, payload) => ({...state, [payload.name]: confirmed() }),
    set: (state, payload) => ({...state, [payload.name]: newState(payload.open, payload.pose)}),
  }
}
