const initialState = () => ({
  currentPanel: 'login',
  panels: {
    login: { title: 'Email', validated: false, next: 'shipping' },
    shipping: { title: 'Delivery', validated: false, next: 'billing', },
    billing: { title: 'Payment', validated: false, }, // next: 'confirm'
    // confirm: { title: 'Confirm Order', validated: true, next: 'complete' },
    // complete: { title: 'Order Complete' },
  },
  totals: {
    subtotal: 0,
    tax: 0,
    total: 0,
  },
  user: {
    hasAccount: false,
    email: '',
    shipping: {},
    billing: {},
  }
})

const validate = (panels, id, validated) => {
  return {...panels, [id]: {...panels[id], validated }}
}

export const checkout = {
  name: 'checkout',
  state: initialState(),
  reducers: {
    changePanel: (state, payload) => ({...state, currentPanel: payload.id }),
    validate: (state, payload) => ({...state, panels: validate(state.panels, payload.id, payload.validated) }),
    'user/success': (state, payload) => {
      const hasAccount = payload.length // temporariy - will need to change on real api
      const user = hasAccount ? payload[0] : {}
      // console.log(payload, user);
      return ({...state, user: {...state.user,
        hasAccount: hasAccount ? true : false,
        shipping: hasAccount ? user.shipping : {},
        billing: hasAccount ? user.billing : {},
      }})
    },
    setEmail: (state, payload) => ({...state, user: {...state.user, email: payload.email}}),
    setShipping: (state, payload) => ({...state, user: {...state.user, shipping: payload.shipping}}),
    setBilling: (state, payload) => ({...state, user: {...state.user, shipping: payload.billing}}),
  },
  effects: {
    checkPanel(payload, state) {
      // 1. Validate the panel
      // 2. If valid - changePanel to next, markasvalid
      // 3. If invalid - display message, show errors
      console.log(payload);
      this.validate({id: payload.id, validated: payload.validated})
      state.currentPanel!==payload.id && this.changePanel({id: payload.next})
    },
  }
}
