import { dispatch } from '@rematch/core'
import { getLineTotal, getLineTotals, getSubtotal, getTax, getTotal } from 'redux/models/example.cart';

const updateTotals = ({ getState }) => next => async action => {
  next(action)

  // Check action contains meta.query
  if (
    (action.type.includes('cart/') || action.type.includes('products/success')) && !action.type.includes('cart/updateTotals')
  ) {
    // Dispatch - needs to be async await - rematch takes a while to get function on load
    await dispatch;
    // console.log('cart action called');

    const items = getState().cart.items
    const products = getState().products.data

    // If products haven't loaded yet, return because we need the prices to do calculations
    if (!products.length) return

    // 1. Calculate Line Totals
    const lineTotals = Object.keys(items).map((item) => {
      return getLineTotal( products.filter(product=>product.id===parseInt(item))[0].price, items[item] )
    })
    // 2. Calculate Subtotal
    const subtotal = getLineTotals(lineTotals)
    // 3. Calculate Tax
    const tax = getTax(0.2, subtotal)
    // 4. Calculate Total
    const totals = [subtotal, tax]
    const total = getTotal(totals)

    // 5. Dispatch update to cart
    dispatch.cart.updateTotals({
      subtotal,
      tax,
      total,
    })
  }
}

export default updateTotals
