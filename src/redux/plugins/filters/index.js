import { filtersState, filtersReducers, filtersActions } from './filters'
import { makeVisibleData } from './filters.selectors'

export default {
  init: (expose) => ({
    onModel(model) {
      if (model.filters) {
        // Cache initial states - so they can be referenced, not recreated - ??? not sure if this works
        const filtersInitial = filtersState()
        model.state = {
          ...model.state,
          ...filtersInitial,
        }

        // Add reducers - mutated - maybe there's a better way?
        model.reducers = {
          ...model.reducers,
          ...filtersReducers(filtersInitial),
        }

        // Add effects/actions - mutated - maybe there's a better way?
        model.effects = {
          ...model.effects,
          ...filtersActions(model.name),
        }

        // Add selectors
        model.selectors = {
          ...model.selectors,
          getVisibleData: makeVisibleData(model.filters), // returns function to process data with active filters
        }

        console.log(model);
        return model
      }
    },
  })
}
