import { createSelector } from 'reselect'
import filterByQuery from 'utils/filterByQuery'

const getData = state => state.data
const getQuery = state => state.query

export const filterData = ({ data, query }, filters) => {
  return data.filter(filterByQuery(query, filters))
}

export const makeVisibleData = (filters) => createSelector(
  [getQuery, getData],
  (query, data) => filterData({query, data}, filters)
)
