import { connectRoutes } from 'redux-first-router'
import history from '../../history'
import { routesMap } from '../../routes'

const { reducer, middleware, enhancer } = connectRoutes(history, routesMap) // yes, 3 redux aspects

// const someReducer = (state, action) => {
//   switch(action.type) {
//     default:
//       return state
//   }
// }

export default {
  init: (expose) => ({
    config: {
      redux: {
        reducers: {
          location: reducer, // doesn't work for some reason
        },
        enhancers: [enhancer], // doesn't work for some reason
        // middlewares: [middleware], // doesn't work for some reason
      }
    },
    middleware: middleware, // works
  })
}
