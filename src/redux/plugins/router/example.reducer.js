import { NOT_FOUND } from 'redux-first-router'

// Example reducer - switches something based on the route - e.g. modal open/closed
export const exampleReducer = (state = null, action = {}) => {
  switch(action.type) {
    case 'HOME':
    case 'PHOTOS':
    case NOT_FOUND:
      return null
    case 'PHOTO':
      return action.payload.id
    default:
      return state
  }
}
