import { apiState, apiReducers } from './api/api.reducers'
import { apiActions } from './api/api.actions'
import { filtersState, filtersReducers, filtersActions } from './filters/filters'

// This is a wrapper around the '@rematch/core' basic object.
// Simply adds the basic state strucutre, actions, and reducers needed to:
// - 1. make API requests
// - 2. set query filters

// When adding to createModel, make sure the reducers and effects have unique names
// - this could be improved by nested namespacing?

const createModel = ({
  name, // name of the model - is used for dispatch[name] e.g: 'photos' -> dispatch.photos.fetch

  state, // additional state
  reducers, // additional reducers
  effects, // additional effects (async actions)

  request, // request object - enables basic data fetch and states
  filters, // filters object - contains filter functions
}) => {
  // Cache initial states - so they can be referenced, not recreated - ??? not sure if this works
  const apiInitial = apiState(false, state) // pass in state to override {data: null} if needed
  const filtersInitial = filtersState()

  return {
    state: {
      ...(request ? apiInitial : {}), // initial state for api data
      ...(filters ? filtersInitial : {}), // initial state for filtering
      ...state,
    },
    reducers: {
      ...(request ? apiReducers(apiInitial) : {}), // reducers for api data - loading, success, error, reset
      ...(filters ? filtersReducers(filtersInitial) : {}), // reducers for filtering - updateFilter, removeFilter
      ...reducers,
    },
    effects: {
      ...(request ? apiActions({ name, request }) : {}), // actions for api request, e.g: fetch()
      ...(filters ? filtersActions(name) : {}), // actions for filtering, e.g: setFilter()
      ...effects,
    }
  }
}

export default createModel
