import apiActions from './api.actions'
import apiMiddleware from './api.middleware'
import { apiReducers, apiState } from './api.reducers'

export default {
  init: (expose) => ({
    onModel(model) {
      console.log(expose);
      if (model.request) {
        // Cache initial states - so they can be referenced, not recreated - ??? not sure if this works
        const apiInitial = apiState(false, model.state) // pass in state to override {data: null} if needed
        model.state = {
          ...model.state,
          ...apiInitial,
        }

        // Add reducers - mutated - maybe there's a better way?
        // const reducers = apiReducers(apiInitial)
        // Object.keys(reducers).forEach((action)=>{
        //   expose.dispatch[model.name][action] = reducers[action]
        // })

        // model.reducers = {
        //   ...model.reducers,
        //   ...apiReducers(apiInitial),
        // }

        // Add effects/actions - mutated - maybe there's a better way?
        // const actions = apiActions({ name: model.name, request: model.request })
        // Object.keys(actions).forEach((action)=>{
        //   expose.effects[`${model.name}/${action}`] = actions[action]
        //   expose.dispatch[model.name][action] = actions[action]
        // })

        // expose.effects[model.name] = {
        //   ...model.effects,
        //   ...apiActions({ name: model.name, request: model.request }),
        // }

        console.log(model, expose);
      }
    },
    middleware: apiMiddleware,
  })
}
