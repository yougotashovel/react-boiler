// Reducer state templates for API requests

export const apiState = (state, overrides) => ({
  ...state, // additional state first, so api request override
  loading: false,
  loaded: false,
  error: false,
  data: null,
  ...overrides, // state defined in model state last, so developer can override or add more
})

export const apiStateLoading = (state) => ({
  ...state, // additional state first, so api request override
  loading: true,
  loaded: false,
  error: false,
})

export const apiStateError = (state, error) => ({
  ...state, // additional state first, so api request override
  loading: false,
  loaded: false,
  error: error,
})

export const apiStateSuccess = (state, data) => ({
  ...state, // additional state first, so api request override
  loading: false,
  loaded: true,
  error: false,
  data: data,
})

export const apiReducers = (resetState) => ({
  // handle state changes with pure functions
  initial: (state, payload) => apiState(state, resetState),
  loading: (state, payload) => apiStateLoading(state),
  success: (state, payload) => apiStateSuccess(state, payload),
  error:   (state, payload) => apiStateError(state, payload),
})

export default apiReducers
