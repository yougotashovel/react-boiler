import { dispatch } from '@rematch/core'

// Using dispatch from '@rematch/core' instead of 'react-redux'
// because it has namespace access to 'success()' and 'error()' for each apiFactory model.

const replaceUrlParams = (url, payload) => {
  // Works the same as React Router urls - '/posts/:id' becomes '/posts/123'
  // Uses the template url in model request, and the payload object - {id: 123}
  var newUrl = url
  for (var item in payload) {
    if (item!=='body' && payload.hasOwnProperty(item)) {
      newUrl = newUrl.replace(`:${item}`,`${payload[item]}`)
    }
  }
  return newUrl
}

const apiMiddleware = (store) => (next) => (action) => {
  next(action);

  // Check if this is an API request.
  if (action.request && action.request.url) {
    const { name, request, payload } = action;

    // Setup Request
    const setup = {
      method: request.method || 'get',
      headers: request.headers,
      body: payload.body ? JSON.stringify(payload.body) : undefined,
    }

    // Parse the URL for any params - e.g: ':id' or ':name'
    const url = replaceUrlParams(request.url, payload)
    console.log(url,setup);

    // Request started - dispatch to redux - sets loading state
    dispatch[name].loading()

    // Do API request
    fetch(url, setup)
      .then( response => response.json())
      .then( data => {
        // Request successful - dispatch data to redux - sets data, removes loading state
        // dispatch({type: 'LOG', payload: data })
        dispatch[name].success(data)
      })
      .catch( error => {
        // Request error - dispatch error to redux - sets error, removes loading state
        // dispatch({type: 'LOG', payload: error })
        dispatch[name].error(error)
      })
  }
};

export default apiMiddleware
