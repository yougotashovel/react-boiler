// Random number between MIN and MAX
export default function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}
