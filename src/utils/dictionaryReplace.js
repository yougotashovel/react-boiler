// Takes string, and object dictionary e.g: { jan: 'January', feb: 'February' ... }
export default function dictionaryReplace(string, dictionary) {
  return string.replace(/(\w+)?/g, function($1, $2) { return dictionary[$2] || $1; });
}
