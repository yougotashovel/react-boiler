// Get first character of a string
export default function firstCharacter(string) {
  return string && string.charAt(0).toUpperCase()
}
