const replaceUrlParams = (url, payload) => {
  // Works the same as React Router urls - '/posts/:id' becomes '/posts/123'
  // Uses the template url in apiFactory request, and the payload object - {id: 123}
  var newUrl = url
  for (var item in payload) {
    if (item!=='body' && payload.hasOwnProperty(item)) {
      newUrl = newUrl.replace(`:${item}`,`${payload[item]}`)
    }
  }
  return newUrl
}

export default replaceUrlParams
