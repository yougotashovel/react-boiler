// Convert 1000000 to 1,000,000
export default function numGroups(num) {
  return num ? num.toLocaleString('en', {useGrouping:true}) : false // doesnt' work in older safari browsers
  // if (num) {
  //   var values = num.toString().split('.');
  //   return values[0].replace(/.(?=(?:.{3})+$)/g, '$&,') + ( values.length == 2 ? '.' + values[1] : '' )
  // } else {
  //   return false
  // }
}
