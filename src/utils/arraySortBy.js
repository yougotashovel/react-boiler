// Sort array of objects - provide key, will sort by that key's value
export default function arraySortBy(key, direction) {
  if (!key) return undefined;
  function desc(a, b) { return (a[key] < b[key]) ? -1 : (a[key] > b[key]) ? 1 : 0 }
  function asc(a, b) { return (a[key] > b[key]) ? -1 : (a[key] < b[key]) ? 1 : 0 }
  return (direction==='desc') ? desc : asc
}
