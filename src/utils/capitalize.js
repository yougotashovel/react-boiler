// Capitalize first letter of string
export default function capitalize(string) {
  return string && string.charAt(0).toUpperCase() + string.slice(1);
}
