// Limit string length, append elipsis or other
export default const limitString = (string, limit = 250, append = "...") => {
  return (string.length>=limit) ? string.substring(0,limit-append.length) + append : string;
  // .replace(/^(.{220}).+/, "$1...")
}
