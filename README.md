#React Boiler
###A collection of re-usable components, simplified composable redux based data patterns, and a unified style system, built to speed up React development and keep UI snappy.

##Quick Start:
`npm run db` activates local json test server

`npm start` runs dev environment (see creat-react-app)

##Overview

###Data
* Model based data management - no more writing actions/reducers for each data set.
* Extend models easily with plugins - add api calls, filters, route queries.
* Consistent data states - loading, loaded, success, error, reset.
* State based routing - allows replaying redux timeline with route changes.

###Styles
* Unified style system - everything declared as JS constants.
* Use your styles as a library - import styles where needed, enforces consistency.
* Better management for: animations, speeds, z-index, color, font, spacing, breakpoints.

###Components
* <Loading> - Wrap around anything, easily use with data states, default fills container or set dimensions.
* <Img> - Extends <img> with: lazy loading, retry on fail, default image, default fills container or set dimensions.
* <Input> - Common fully stylable inputs, with error/success states, hook into Formik or other libraries.
* <Button> - Standardised button styles and states, works similar to other libraries.
* <Link nav> - Abstracted <a> for internal links, and stylable nav links.
* <Text type="h1"> - Quickly change text types, great when moving around/editing layout, adds your style library.
* <View> - Like react-native, extends <div> with ability to use your style library.
* <Meta> - Extending Helmet for page meta/titles, set defaults.
* <Modal> - Basic animated modals with confirm/cancel actions, can have routes.
* withKeys - HOC for easily adding keyboard actions to any component.

###Common Patterns
* Windowing - long lists with many components create lag, windowing lists only shows what's necessary to keep the UI fast.
* List Filters - Define data filters in the model, then easily use them as actions in the interface.
* URL Queries - Turn on URL Queries for any filter defined in the model.
* Route Transitions - As route is simply state, transition can be applied like regular component state transitions.
* Shop/Cart - consistent snappy front-end shop management, built rapidly using only the above.
* Checkout - simplified checkout process, validation, easily rearrangeable/extendable, navigate away without losing state.


##Next steps in this project:   
* Abstract the styles to it's own library, which could be imported on multiple projects, enforcing consistency on things that scale.
* Finish route transitions, following game-design interface patterns.
* Test Apollo for loading data.
* Improve documentation.

---
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).
